-- If AUTHENTICATION and SQL AUTHORISATION are both enabled (YES),
-- only the database owner can shut down or drop the database,
-- encrypt it, reencrypt it with a new boot password or new
-- encryption key, or perform a full upgrade.
--
-- https://builds.apache.org/job/Derby-docs/lastSuccessfulBuild/artifact/trunk/out/ref/index.html
--
-- SQL vs Derby Features:
--   https://cwiki.apache.org/confluence/display/DERBY/SQLvsDerbyFeatures
--
-- Build one clause at a time:
--  SELECT
--  FROM
--  WHERE
--  GROUP BY
--  HAVING
--  SELECT
--  ORDER BY
--
-- Coronel, C & Morris, S 2018, Database Systems: Design, Implementation, and Management, 13th edn, p.318, Cengage Learning, Boston, MA, USA.
-- Query Optimisation, chpt 11, p.515.
--
-- TODO: VARCHAR(BYTES), CREATE TRIGGER LOGIN, TEMPLATE READONLY
--
-- CREATE TRIGGER triggerName
-- { AFTER | NO CASCADE BEFORE }
-- { INSERT | DELETE | UPDATE [ OF columnName [ , columnName ]* ] }
-- ON tableName
-- [ referencingClause ]
-- [ FOR EACH { ROW | STATEMENT } ] [ MODE DB2SQL ]
-- [ WHEN ( booleanExpression ) ]
-- triggeredSQLStatement
--
--
-- I need a <?> that checks (base.name, base.login) before
-- attempt base.name connection (a quick short circuit).
--
-- I need a trigger that updates base.login boolean from
-- (base.pk, base.name) after base.name connected.
--
-- USAGE:
-- $ java -jar $DERBY_HOME/lib/derbyrun.jar ij
-- ij> maximumdisplaywidth 30;
-- ij> connect 'jdbc:derby:pbe;create=true';
-- ij> run 'pbedb.sql';
-- ij> show tables in APP;
-- ij> describe APP.BASE;
-- ij> select * from APP.BASE;
-- ij> select BA_NAME, BA_HASH from APP.BASE where BA_NAME = 'DUNCAN';
-- ij> connect 'jdbc:derby:pbe;shutdown=true';
-- ERROR 08006: Database 'pbe' shutdown.
-- ij> connect 'jdbc:derby:;shutdown=true';
-- ERROR XJ015: Derby system shutdown.
-- ij> exit;
--
-- \************************************************************\

AUTOCOMMIT OFF;

-- \************************************************************\
--  constraint checking (primary key, unique key, foreign key, check)
--  GENERATED ALWAYS AS IDENTITY ( START WITH 1, INCREMENT BY 1 ) <- is unique, not indexed, non-insertable

CREATE TABLE BASE (
  b_id    INTEGER      NOT NULL,
  b_name  VARCHAR(128) NOT NULL,
  b_hash  VARCHAR(396) NOT NULL,
  b_login BOOLEAN      NOT NULL,
  b_time  TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP,
--  b_timeread
--  b_timeupdated

  PRIMARY KEY ( b_id ),

  UNIQUE ( b_name ), -- <- does error code confirm another users database name?

  CONSTRAINT b_ck1 CHECK ( 7 < LENGTH( b_name ) AND 129 > LENGTH( b_name ) ), -- need to confirm size
--                                                                          ^-- Warning: too fragile for humans
);

-- \************************************************************\

CREATE TABLE STANDARD (
  s_id      INTEGER      NOT NULL,
  s_name    VARCHAR(150) NOT NULL,
  s_role    VARCHAR(396),  -- need to deal with nulls DEFAULT 'StandardDTOLenses'
  s_address VARCHAR(128),
  s_access  VARCHAR(396),
  s_text    CLOB(1M),
  s_time    TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP,

  PRIMARY KEY ( sd_id ),

  CONSTRAINT sd_ck1 CHECK ( 0 < LENGTH( sd_name ) )
);

-- \************************************************************\

/*
 * logic in the application layer to set BASE.ba_login = true
 *
 * check ba_login is false
 * set ba_login when ba_hash matches users credentials
 * set ba_time to user accessing ba_hash
 *
 * what about failed attempts
 */
CREATE TRIGGER base_login  -- TEST: needed
  AFTER UPDATE OF BASE.login
  ON BASE
  FOR EACH ROW
  WHEN ( BASE.ba_login )
  UPDATE BASE
         SET BASE.ba_time = CURRENT_TIMESTAMP
  WHERE OLD.BASE.ba_id = NEW.BASE.ba_id;


-- \************************************************************\

INSERT INTO base ( ba_name, ba_hash, ba_login, ba_time )  VALUE ('NAME'  , 'ALGORITHM:ITERATIONS:SALT:HASH', TRUE , DEFAULT);
INSERT INTO base ( ba_name, ba_hash, ba_login, ba_time )  VALUE ('DUNCAN', 'ALGORITHM:ITERATIONS:SALT:HASH', TRUE , DEFAULT);
INSERT INTO base ( ba_name, ba_hash, ba_login, ba_time )  VALUE ('JAMES' , 'HASH:SALT:ITERATIONS:ALGORITHM', TRUE , DEFAULT);
INSERT INTO base ( ba_name, ba_hash, ba_login, ba_time )  VALUE ('KIRK'  , 'SALT:HASH:ALGORITHM:ITERATIONS', FALSE, DEFAULT);

-- \************************************************************\

-- CREATE TABLE STANDARD AS SELECT * FROM TEMPLATE WITH NO DATA;

-- \************************************************************\

COMMIT;
