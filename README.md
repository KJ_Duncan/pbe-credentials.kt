A proof of concept for the [mailer](https://bitbucket.org/KJ_Duncan/mailer.kt/src/master/) repository.  
  
```
USE-CASE:  Credential Storage
REASONING: Exposing numerous projects, clients, colleagues, or users
           secure credentials system wide at all times is incomprehensible.

GOAL:      Standardised IO
           Encrypted
           Databases (1..n)

Connection Mode:
  Multi-Connection - Create individual connections within a single application and use the 
                     appropriate connection for each JDBC request. The connections can all be to 
                     the same database, or can be to different databases in the same Derby system.
```


----


Current readings:  

+  Apache 2020, _Apache Derby: Documentation_, Apache Derby, v10.15 Manuals, The Apache DB Project, viewed 03 March 2021, https://db.apache.org/derby/manuals/index.html#docs_10.15
+  Coronel, C & Morris, S 2018, Database Systems: Design, Implementation, and Management, 13th edn, p.318, Cengage Learning, Boston, MA, USA.
+  Gradle Inc 2021, _Gradle User Manual_, version 6.8.3, viewed 03 March 2021, https://docs.gradle.org/current/userguide/userguide.pdf
+  Oracle 2020, _java.sql_, Java SE 13 & JDK 13, viewed 26 August 2020, https://docs.oracle.com/en/java/javase/13/docs/api/java.sql/java/sql/package-summary.html  
+  Oracle 2020, _JDBC Basics_, The Java Tutorials, Java Documentation, viewed 26 August 2020, https://docs.oracle.com/javase/tutorial/jdbc/basics/index.html  
+  Tudose, C Tahchiev, P Leme, F Massol, V & Gregory, G 2020, _Testing database applications_, JUnit in Action, 3rd edn, chpt 19, Manning Publications, viewed 26 August 2020, https://www.manning.com/books/junit-in-action-third-edition  


  
-  Skeges 2018, Road Trip, My Own Mess, viewed 30 May 2021, `https://open.spotify.com/track/1e4GeY6uIq9LbJrYrRip6c?si=b1ec4d9c9f334614`  


----


Not yet production ready, outstanding tasks:  


#### Doing


- [ ] Database


#### Todo


- [ ] Benchmark
- [ ] Documentation
- [ ] Exceptions
- [ ] Internationalisation
- [ ] Logging
- [ ] Login
- [ ] Message Bus
- [ ] Module System
- [ ] Test suite
- [ ] Validation console/passphrase


#### Done


- [x] CheckCredentials
- [x] SecureHash


----


Login:  

+  [javax.security.auth.login.LoginContext](https://docs.oracle.com/en/java/javase/13/docs/api/java.base/javax/security/auth/login/LoginContext.html)
+  [javax.security.auth.login.Configuration](https://docs.oracle.com/en/java/javase/13/docs/api/java.base/javax/security/auth/login/Configuration.html)
+  [javax.security.auth.spi.LoginModule](https://docs.oracle.com/en/java/javase/13/docs/api/java.base/javax/security/auth/spi/LoginModule.html)
+  [javax.security.auth.callback.CallbackHandler](https://docs.oracle.com/en/java/javase/13/docs/api/java.base/javax/security/auth/callback/CallbackHandler.html)


![](plantuml/png/pbe_credentials.png)


----


DerbyDB:  
@see this repositories `derby-db.txt` for details.  
 
+  [Configuring NATIVE authentication](http://db.apache.org/derby/docs/10.15/security/index.html)
 

```
-- If AUTHENTICATION and SQL AUTHORISATION are both enabled (YES),
-- only the database owner can shut down or drop the database,
-- encrypt it, reencrypt it with a new boot password or new
-- encryption key, or perform a full upgrade.

-- \************************************************************\
   +--------------------+
   |      BASE          |
   +--------------------+
   |PK | b_id           |
   +--------------------+
   |   | b_name         |
   |   | b_hash         |
   |   | b_timestamp    |
   |   | b_login        |
   +--------------------+

   +--------------------+
   |      STANDARD      |
   +--------------------+
   |PK | s_id           |
   +--------------------+
   |   | s_name         |
   |   | s_role         |
   |   | s_address      |
   |   | s_access       |
   |   | s_text         |
   |   | s_time         |
   +--------------------+

-- \************************************************************\

   +------- JVM -------+
   |                   |
   |  +-------------+  |
   |  | application |  |
   |  +-----+-------+  |
   |        |          |
   |    +---+----+     |
   |    |  JDBC  |     |
   |    +--------+     |
   |    |  DERBY |     |
   |    +---+----+     |
   |        |          |
   +--------+----------+
            |
            |   +----------+
            \___| database |
                | files    |
                +----------+

-- \************************************************************\
```


----


![](plantuml/png/Data-Dictionary_PBE-Project.jpg)


----
 

Jshell:  

+  [module jdk.jshell](https://docs.oracle.com/en/java/javase/15/docs/api/jdk.jshell/module-summary.html)
+  [package jdk.jshell](https://docs.oracle.com/en/java/javase/15/docs/api/jdk.jshell/jdk/jshell/package-summary.html)
+  [jline-3](https://github.com/jline/jline3)


---


Validator (regex works on type String not on type CharArray will need to use a boolean in range 'a'..'z' function):  

+  [java.lang.Character](https://docs.oracle.com/en/java/javase/15/docs/api/java.base/java/lang/Character.html)
+  [java.util.regex](https://docs.oracle.com/en/java/javase/15/docs/api/java.base/java/util/regex/package-summary.html)
+  [java.util.regex.Pattern](https://docs.oracle.com/en/java/javase/15/docs/api/java.base/java/util/regex/Pattern.html)
+  [java.util.regex.Matcher](https://docs.oracle.com/en/java/javase/15/docs/api/java.base/java/util/regex/Matcher.html)
+  [java.util.Formatter](https://docs.oracle.com/en/java/javase/15/docs/api/java.base/java/util/Formatter.html)


----
 

Exceptions:  

+  [PatternSyntaxException](https://docs.oracle.com/en/java/javase/15/docs/api/java.base/java/util/regex/PatternSyntaxException.html)
+  [IOError](https://docs.oracle.com/en/java/javase/15/docs/api/java.base/java/io/IOError.html)
+  [IllegalFormatException](https://docs.oracle.com/en/java/javase/15/docs/api/java.base/java/util/IllegalFormatException.html)


---- 


Logger:  

+  [java.util.logging](https://docs.oracle.com/en/java/javase/15/docs/api/java.logging/java/util/logging/package-summary.html)
+  [java.util.logging.Logger](https://docs.oracle.com/en/java/javase/15/docs/api/java.logging/java/util/logging/Logger.html)
+  [java.util.logging.SimpleFormatter](https://docs.oracle.com/en/java/javase/15/docs/api/java.logging/java/util/logging/SimpleFormatter.html)
+  [Theory: Introduction to logging](https://hyperskill.org/learn/step/5538)
+  [Theory: Standard logger](https://hyperskill.org/learn/step/6001)


----


References:  

+  Corda 2020, _ByteArrays.kt & EncodingUtils.kt_, open-source blockchain, release 4.6, viewed 03 June 2020, https://github.com/corda/corda/blob/release/os/4.6/core/src/main/kotlin/net/corda/core/utilities/
+  Duncan, K 2020, _jse-security-guide_, preliminary work, viewed 03 June 2020, https://bitbucket.org/KJ_Duncan/jse-security-guide/src/master/
+  Mayoral, F 2013, Instant Java Password and Authentication Security, Packt Publishing, viewed on 08 June 2020, https://www.packtpub.com/application-development/instant-java-password-and-authentication-security-instant
+  Studer, E 2020, _gradle-credentials-plugin_, nu.studer.credentials, viewed 03 June 2020, https://github.com/etiennestuder/gradle-credentials-plugin
+  Studer, E 2020, _gradle-jooq-plugin_, nu.studer.jooq, viewed 03 June 2020, https://github.com/etiennestuder/java-ordered-properties
  
