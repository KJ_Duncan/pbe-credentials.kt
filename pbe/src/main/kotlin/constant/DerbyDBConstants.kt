package constant

/*
   The default framework is embedded
   jdbc:derby:[subsubprotocol:][databasName][;attributes]*
*/
const val DB_MEMORY: String   = "memory"
const val DB_EMBEDDED: String = "embedded"
const val DB_PROTOCOL: String = "jdbc:derby"
const val DB_DRIVER: String   = "org.apache.derby.jdbc.EmbeddedDriver"

const val DB_IDENTITY: String = "mayoralDB"

const val DB_CREATE_TRUE: String  = "create=true"
const val DB_CREATE_FALSE: String = "create=false"

const val DB_SHUTDOWN_TRUE: String = "shutdown=true"

const val DB_DATA_ENCRYPTION_TRUE: String = "dataEncryption=true"
/* DES (the default) (56 bits) */
const val DB_ENCRYPTION_ALGORITHM: String = "encryptionAlgorith="
const val DB_ENCRYPTION_PROVIDER: String  = "encryptionProvider="
/* The boot password should have at least as many characters as number of
   bytes in the encryption key (56 bits=8 bytes, 168 bits=24 bytes, 128 bits=16 bytes).
   The minimum number of characters for the boot password allowed by Derby is eight. */
const val DB_BOOT_PASSWORD: String  = "bootPassword="
const val DB_ENCRYPTION_KEY: String = "encryptionKey="
/* To specify the AES encryption algorithm with a key length other than the
   default of 128 (192 or 256) */
const val DB_ENCRYPTION_KEY_LENGTH: String = "encryptionKeyLength="
