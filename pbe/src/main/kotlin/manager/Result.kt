package manager

import java.sql.SQLException

/**
 * Result data type consolidates and centralises the error-handling logic instead of throwing an
 * exception. Higher-order functions encapsulate common patterns of handling and propagating
 * those errors. The Result data type is completely type safe error-handling strategy
 * (Vermeulen, M et al. 2020, p.57).
 *
 ***
 * Access the getSQLState and getMessage methods to view the SQLState and error messages,
 * and use getErrorCode to see the error code. The error code defines the severity of the
 * error and is not unique to each exception.
 *
 * * sqle.getSQLState()
 * * sqle.getErrorCode()
 * * sqle.getMessage()
 *
 * * Result.fold({ ifSqlState(it) }, { ifFailure(it) }, { ifSuccess(it) })
 *
 * @author Apache Derby 2020, SQL error messages and exceptions,
 *         viewed 01 July 2020, http://db.apache.org/derby/docs/10.15/ref/
 *
 * @author Arrow FX 2020, IO.kt, package arrow.fx, viewed 01 July 2020,
 *         https://github.com/arrow-kt/arrow-fx/blob/master/arrow-fx/src/main/kotlin/arrow/fx/IO.kt
 *
 * @author resutl4k 2020, result.kt, package com.natpryce, viewed 02 July 2020,
 *         https://github.com/npryce/result4k/blob/master/src/main/kotlin/com/natpryce/result.kt
 *
 * @author Saumont, P-Y 2019, Result.kt, package com.fpinkotlin.common,
 *         The Joy of Kotlin, Manning Publications, viewed 02 July 2020,
 *         https://github.com/pysaumont/fpinkotlin/blob/master/fpinkotlin-parent/fpinkotlin-common/src/main/kotlin/com/fpinkotlin/common/Result.kt
 */
class ForResult private constructor() { companion object }

interface HK<out F, out A>
typealias HK2<F, A, B>   = HK<HK<F, A>, B>
typealias ResultOf<S, E> = HK2<ForResult, S, E>

// -- down casting to a concrete type ----------------------------------------------------------------
@Suppress("UNCHECKED_CAST", "NOTHING_TO_INLINE")
inline fun <S, E> ResultOf<S, E>.fix(): Result<S, E> = this as Result<S, E>

// ---------------------------------------------------------------------------------------------------
sealed class Result<out S, out E> : ResultOf<S, E> {
  abstract fun forEach(onSuccess:  (S) -> Unit = {},
                       onFailure:  (E) -> Unit = {},
                       onSqlState: (SQLException) -> Unit = {})

  data class Success<S>(val value: S) : Result<S, Nothing>() {
    override fun forEach(onSuccess: (S) -> Unit,
                         onFailure: (Nothing) -> Unit,
                         onSqlState: (SQLException) -> Unit)
                         { onSuccess(value) } }

  data class Failure<E>(val reason: E) : Result<Nothing, E>() {
    override fun forEach(onSuccess: (Nothing) -> Unit,
                         onFailure: (E) -> Unit,
                         onSqlState: (SQLException) -> Unit)
                         { onFailure(reason) } }

  data class SQLState<E : SQLException>(val code: E) : Result<Nothing, E>() {
    override fun forEach(onSuccess: (Nothing) -> Unit,
                         onFailure: (E) -> Unit,
                         onSqlState: (SQLException) -> Unit)
                         { onSqlState(code) } }
}

// ---------------------------------------------------------------------------------------------------
typealias Success<S>  = Result.Success<S>
typealias Failure<E>  = Result.Failure<E>
typealias SQLState<E> = Result.SQLState<E>

// ---------------------------------------------------------------------------------------------------
/* flatMap ∷ f s → (s → f r) → f r */
inline fun <S, R, E> ResultOf<S, E>.flatMap(fn: (S) -> Result<R, E>): Result<R, E> =
  fix().let {
    when (it) {
      is Success  -> fn(it.value)
      is Failure  -> it
      is SQLState -> it
    }
  }

/* map ∷ f s → (s → r) → f r */
inline fun <S, R, E> ResultOf<S, E>.map(fn: (S) -> R): Result<R, E> =
  flatMap { s -> Success(fn(s)) }

/* lift ∷ (s → r) → f s → f r */
fun <S, R, E> liftResult(fn: (S) -> R): (ResultOf<S, E>) -> Result<R, E> =
  { rs: ResultOf<S, E> -> rs.map(fn) }

/* ap ∷ f (s → r) → f s → f r */
infix fun <S, R, E> ResultOf<(S) -> R, E>.ap(rs: Result<S, E>): Result<R, E> =
  flatMap { f: (S) -> R -> rs.map(f) }

/* pure ∷ s → f s */
fun <S, E> pureResult(s: S): Result<S, E> =
  Success(s)

// TEST::result({it in 0..size}, {it::length}) { Result<Success(user = input in range), Failure(msg = "input length > column width")> }
// @see https://hackage.haskell.org/package/base-4.14.0.0/docs/Prelude.html#v:either
/* result ∷ (s → r) → (e → r) → f s e → r */
fun <S, R, E> result(fs: (S) -> R, fe: (E) -> R): (Result<S, E>) -> R =
  { se ->
    when (se) {
      is Success  -> fs(se.value)
      is Failure  -> fe(se.reason)
      is SQLState -> fe(se.code)
    }
  }

// ---------------------------------------------------------------------------------------------------
inline fun <A> resultFrom(block: () -> A): Result<A, Throwable> =
  try { Success(block()) }
  catch (sql: SQLException) { SQLState(sql) }
  catch (ex: Exception) { Failure(ex) }
