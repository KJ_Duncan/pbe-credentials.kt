package manager

/*
   @author Vermeulen, M Bjarnason, R & Chiusano, P 2020, Parser combinators, Functional Programming in Kotlin,
           Manning Publications, viewed 03 July 2020, https://www.manning.com/books/functional-programming-in-kotlin

   https://github.com/fpinkotlin/fpinkotlin/blob/master/src/main/kotlin/chapter9/sec6_5/listing.kt

   interface Location

   data class ParseError(val stack: List<Pair<Location, String>>)

   override fun string(s: String): Parser<String> =
     { input: String ->
       if (input.startsWith(s))
         Success(s)
       else Failure(Location(input).toError("Expected: $s"))
     }

   private fun Location.toError(msg: String) =
     ParseError(listOf(Pair(this, msg)))

   infix fun <S> Parser<S>.or(fn: () -> Parser<S>): Parser<S>

   val p1: Parser<String>
   val p2: Parser<String>
   val p3: p1 or p2

   object ParseError
   @see https://github.com/fpinkotlin/fpinkotlin/blob/master/src/main/kotlin/chapter9/sec6_1/listing.kt
   typealias Parser<S> = (String) -> Result<S, ParseError>
 */

/*
   get user input,
   check/convert type to column (string, byteArray, ...),
   measure length against know column width,
   fallback to default column input

take a copy of users stdin::Pair(left, right)
preform parser validation on copy branch, if
branch succeeds then commit original branch
else review molested copy against its original
to determine fallback operation.

eight column/types, six are nullable, one is default.
A user may not require input for all columns, derby-db
requires a complete row on insert. An alternation pattern?

@see https://github.com/Kotlin/kotlinx-cli
     https://github.com/Kotlin/kotlin-interactive-shell


*/
typealias ParserA<A> = (String) -> List<Pair<A, String>>

// dummy function, just thinking, could flatmap to map
fun stdin(): ParserA<Result<String, String>> =
  { input ->
    if (input.length in 1..20) listOf(Pair(Success(input), input))
    else listOf(Pair(Result.Failure(input), input))
  }

/* map ∷ f a → (a → b) → f b */
fun <A> ParserA<A>.map(fn: (A) -> String): ParserA<String> =
  { str: String ->
    when {
      str.isEmpty() -> emptyList()
      else -> {
        val input = this(str)
        val a = input[0].first
        listOf(Pair(fn(a), str))
      }
    }
  }

fun <A> pure(a: A): ParserA<A> =
  { s -> listOf(Pair(a, s)) }

// fun <A> ParserA<(A) -> String>.ap(px: ParserA<A>): ParserA<String>


/*
@author Hutton, G 2016, 13. Monadic parsing, Programming in Haskell, second ed.

newtype Parser a = P (String -> [(a, String)])

instance Functor Parser where
  -- fmap :: (a -> b) -> f a -> f b
  fmap g p = P (\input -> case p input of
                  [] -> []
                  [(v, out)] -> [(g v, out)])

instance Applicative Parser where
  -- pure :: a -> Parser a
  pure v = P (\input -> [(v, input)])

  -- <*> :: Parser (a -> b) -> Parser a -> Parser b
  pg <*> px = P (\input -> case pg input of
                  [] -> []
                  [(g, out)] -> (fmap g px) out)

instance Monad Parser where
  -- (>>=) :: Parser a -> (a -> Parser b) -> Parser b
  p >>= f = P (\input -> case p input of
                  [] -> []
                  [(v, out)] -> (f v) out)

instance Alternative Parser where
  -- empty :: Parser a
  empty = P (\input -> [])

  -- (<|>) :: Parser a -> Parser a -> Parser a
  p <|> q = P (\input -> case p input of
                  [] -> q input
                  [(v, out)] -> [(v, out)])

item :: Parser Char
item = P (\input -> case input of
            [] -> []
            (x:xs) -> [(x, xs)])
*/

/*
LAWS

run(char(c), c.toString()) == Success(c)
run(string(s), s) == Success(s)

run(or(string("abra"), string("cadabra")), "abra") == Success("abra")
run(or(string("abra"), string("cadabra")), "cadabra") == Success("cadabra")

run("abra" or "cadabra", "abra") == Success("abra")
run("abra" or "cadabra", "cadabra") == Success("cadabra")

run(succeed(a), s) == Success(a)

map(p) { a -> a } == p

*/
