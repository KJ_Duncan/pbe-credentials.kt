package manager

/* -----------------------------------------------------------------------------------------------
   Haskell bind (>>=) is flatMap
     where `f` is type Functor -> Applicative -> Monad

   (>>=) :: (Monad f) => f t -> (t -> f r) -> f r
   (>>=) :: (State s) t -> (t -> (State s) r) -> (State s) r

   flatMap :: State s t -> (t -> State s r) -> State s r

   @see https://wiki.haskell.org/wikiupload/d/df/Typeclassopedia-diagram.png
   @see https://www.stackage.org/haddock/lts-16.5/base-4.13.0.0/GHC-Base.html
   @see https://strabismicgobbledygook.wordpress.com/2010/03/06/a-state-monad-tutorial/
   ----------------------------------------------------------------------------------------------- */
typealias State<S, T> = (S) -> Pair<S, T>

/* flatMap ∷ f t → (t → f r) → f r */
fun <S, T, R> State<S, T>.flatMap(fn: (T) -> State<S, R>): State<S, R> =
  { s: S ->
    val (s1, t) = this(s)
    fn(t).invoke(s1)
  }

// https://en.wikibooks.org/wiki/Haskell/Understanding_monads/State
fun <S, T, R> compose(st: State<S, T>, fn: (T) -> State<S, R>): State<S, R> = TODO()


/* map ∷ f t → (t → r) → f r */
fun <S, T, R> State<S, T>.map(fn: (T) -> R): State<S, R> =
  { s: S ->
    val (s1, t) = this(s)
    Pair(s1, fn(t))
  }

/* pure ∷ t → f t */
fun <S, T> pureState(t: T): State<S, T> =
  { s -> s to t }

/* ap ∷ f (t → r) → f t → f r */
fun <S, T, R> State<S, (T) -> R>.ap(st: State<S, T>): State<S, R> =
  { s: S ->
    val (s1, f) = this(s)
    val (s2, t) = st(s1)
    Pair(s2, f(t))
  }

/* lift ∷ (t → r) → f t → f r */
fun <S, T, R> liftState(fn: (T) -> R): (State<S, T>) -> State<S, R> =
  { st -> st.map(fn) }
