package manager

import domain.SDAddress
import domain.SDBinary
import domain.SDHash
import domain.SDName
import domain.SDRole
import domain.SDSecret
import domain.SDText
import domain.SDTimeStamp
import domain.DTO
import domain.StandardDTO

/**
 * Element Characteristics
 *   Entity:
 *     - Has an identity
 *     - Passes through multiple states in the lifecycle
 *     - Usually has a definite lifecycle in the business
 *
 *   Value object:
 *     - Semantically immutable
 *     - Can be freely shared across entities
 *
 *   Service:
 *     - More macro-level abstraction than entity or value object
 *     - Involves multiple entities and value objects
 *     - Usually models a use case of the business
 *
 * @author Ghosh, D 2016, Functional and Reactive Domain Modeling,
 *         Manning Publications, viewed 19 May 2020,
 *         https://www.manning.com/books/functional-and-reactive-domain-modeling
 *
 * @author Kmett, E et al. 2020, Lens: Lenses, Folds, and Traversals,
 *         viewed 11 July 2020, http://hackage.haskell.org/package/lens
 */
interface Lens<O : DTO, V : Any> {
  fun get(): (O) -> V
  fun set(): (O, V) -> O

  companion object {
    operator fun <O : DTO, V : Any> invoke(get: (O) -> V, set: (O, V) -> O): Lens<O, V> =
      object : Lens<O, V> {
        override fun get(): (O) -> V = { o -> get(o) }
        override fun set(): (O, V) -> O = { o, v -> set(o, v) }
      }

    fun <O : DTO, I : DTO, V : Any> compose(outer: Lens<O, I>, inner: Lens<I, V>): Lens<O, V> =
      Lens(
        get = outer.get() andThen inner.get(),
        set = { o, v -> outer.set()(o, inner.set()(outer.get()(o), v)) }
      )
  }
}

/* First and last types are none null,
   (v!!) was a poor design choice as
   an uncaught null point exception
   will be thrown on a erroneous code or
   input. Side effects lead to increased
   complexity and make the behaviour of
   a program harder to reason about.
   Null is still not a type. */
interface StandardDTOLenses {

  fun nameLens() = Lens<StandardDTO, String>(
    get = { o -> o.name.value },
    set = { o, v -> o.copy(name = SDName(v)) }
  )

  fun secretLens() = Lens<StandardDTO, String>(
    get = { o -> o.secret.value },
    set = { o, v -> o.copy(secret = SDSecret(v)) }
  )

  fun roleLens() = Lens<StandardDTO, String>(
    get = { o -> o.role.value },
    set = { o, v -> o.copy(role = SDRole(v)) }
  )

  fun hashLens() = Lens<StandardDTO, String>(
    get = { o -> o.hash.value },
    set = { o, v -> o.copy(hash = SDHash(v)) }
  )

  fun addressLens() = Lens<StandardDTO, String>(
    get = { o -> o.address.value },
    set = { o, v -> o.copy(address = SDAddress(v)) }
  )

  fun binaryLens() = Lens<StandardDTO, ByteArray>(
    get = { o -> o.binary.value },
    set = { o, v -> o.copy(binary = SDBinary(v)) }
  )

  fun textLens() = Lens<StandardDTO, String>(
    get = { o -> o.text.value },
    set = { o, v -> o.copy(text = SDText(v)) }
  )

  fun timeLens() = Lens<StandardDTO, String>(
    get = { o -> o.time.value },
    set = { o, v -> o.copy(time = SDTimeStamp(v)) }
  )
}

/* Function composition Left -> Right.
 * @author Arrow-kt, composition.kt, arrow-core, package arrow.syntax.function, viewed 09 July 2020,
 *         https://github.com/arrow-kt/arrow-core/blob/master/arrow-syntax/src/main/kotlin/arrow/syntax/function/composition.kt
 */
inline infix fun <A, B, C> ((A) -> B).andThen(crossinline f: (B) -> C): (A) -> C = { a: A -> f(this(a)) }


/*
inline infix fun <P1, P2, IP, R> ((P1, P2) -> IP).andThen(crossinline f: (IP) -> R) = forwardCompose(f)
inline infix fun <IP, R> (() -> IP).andThen(crossinline f: (IP) -> R): () -> R = forwardCompose(f)
inline infix fun <A, B, C> ((A) -> B).forwardCompose(crossinline f: (B) -> C): (A) -> C = { p1: A -> f(this(p1)) }
inline infix fun <P1, P2, IP, R> ((P1, P2) -> IP).forwardCompose(crossinline f: (IP) -> R) = { p1: P1, p2: P2 -> f(this(p1, p2)) }
inline infix fun <IP, R> (() -> IP).forwardCompose(crossinline f: (IP) -> R): () -> R = { f(this()) }
inline infix fun <IP, R, P1> ((IP) -> R).compose(crossinline f: (P1) -> IP): (P1) -> R = { p1: P1 -> this(f(p1)) }
 */

