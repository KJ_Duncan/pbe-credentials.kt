import java.lang.System.err
import kotlin.system.exitProcess

object EntryPoint {
  /* NOTE::main see example.DerbyIO.kt, example.NativeAuthenticationExample.java for compilation and command line usage */
  @JvmStatic
  fun main(args: Array<String>) {
    /* java -Djava.security.debug=help EntryPoint

      wrap in correct logic
      base = Conglomerate.Identity() <-- unary so needs to be a singleton object

      the classification of a subordinate database is at the users discretion
      user = Conglomerate.Credentials() <-- multi-tenancy so needs to be a class

      NOTE::ImmutableMap and Asynchronous Flow
            https://github.com/Kotlin/kotlinx.collections.immutable/blob/master/core/commonMain/src/ImmutableMap.kt
            https://kotlinlang.org/docs/reference/coroutines/flow.html

      ImmutableMap<RTToken, DTO> for asynchronous operations to derby-db,
      Flows are cold streams which can be transformed with operators,
      as done with collections and sequences. A flow can be cancelled.

     // ---------------------------------------------------------------------------------------------------

       can box on the Conglomerate type
       can pattern match on sealed class Conglomerate with
         = when (type) {
           is Identity    ->
           is Credentials ->
         }

     // ---------------------------------------------------------------------------------------------------

       typealias Writer<S, T> = (S) -> Pair<T, String>
       can log on uuid

     // ---------------------------------------------------------------------------------------------------

       typealias State<S, T> = (S) -> Pair<S, T>
       can handle privileges

       I want to get from EntryPoint to SQLException and back to EntryPoint
         what I get back Tuple(s, t)
                           where s = Conglomerate
                                 t = Boolean
                           t is dependant on the SQLException raised by derby

     // ---------------------------------------------------------------------------------------------------

       typealias Parser<S> = (String) -> Result<S, ParseError>
       can parse user input

     // ---------------------------------------------------------------------------------------------------

       EntryPoint -> Conglomerate -> LoginContext -> CallbackHandler            -> LoginModule -> DerbyDB      -> SQLException
                  -> Identity                     -> IdentityCallbackHandler    -> Login       -> SysAdm
                  -> Credentials                  -> CredentialsCallbackHandler -> Login       -> Subordinates

       So lets say all this works, I receive back an object that I still need to do database queries on, so this does not solve that.

       So the question is what operations: PreparedStatement(), see example.NativeAuthenticationExample.java
          dependency injection = object : Typeclass<Conglomerate>
          where Typeclass = interface DaoDatabase, DaoOperations, DaoFetcher

       call the necessary functions/methods on the Conglomerate object

     // ---------------------------------------------------------------------------------------------------

       Catch and wrap exceptions
         Result<Success, Failure>
     */
    exitProcess(0)
  }
}
