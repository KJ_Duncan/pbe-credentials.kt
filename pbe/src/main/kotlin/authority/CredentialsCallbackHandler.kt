package authority

import java.io.BufferedReader
import java.io.InputStreamReader
import java.lang.System.console
import java.lang.System.err
import java.lang.System.`in`
import javax.security.auth.callback.Callback
import javax.security.auth.callback.CallbackHandler
import javax.security.auth.callback.NameCallback
import javax.security.auth.callback.PasswordCallback
import javax.security.auth.callback.UnsupportedCallbackException

internal class CredentialsCallbackHandler : CallbackHandler {
  /*
   * To compare hashes (Authentication), follow the given steps:
   *   1. Get the password as a char array.
   *   2. Get the stored password with its iterations and salt. (Derby embedded db)
   *   3. Create a password-based encryption key spec.
   *   4. Create a key factory.
   *   5. Generate the hash formatted with the salt and the iterations.
   *   6. Compare the generated hash with the stored one.
   */
  override fun handle(callbacks: Array<out Callback>): Unit =
    callbacks.forEach { callback: Callback ->
      when (callback) {
        is NameCallback -> {
          err.print(callback.prompt)
          err.flush()

          callback.name = BufferedReader(InputStreamReader(`in`)).readLine()
        }
        is PasswordCallback -> {
          err.print(callback.prompt)
          err.flush()
          /* passphrase does the spread operator create an intermediate collection?
             if the "array already exists" the spread operator passes its contents
             to a function: console -> charArrayOf
             was: charArrayOf(*console().readPassword())
             which created an additional object. */
          callback.password = console().readPassword()
        }
        else -> throw UnsupportedCallbackException(callback, "Unsupported Callback Type")
      }
    }
}
