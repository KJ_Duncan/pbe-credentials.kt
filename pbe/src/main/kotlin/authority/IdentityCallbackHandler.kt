package authority

import java.io.BufferedReader
import java.io.InputStreamReader
import java.lang.System.console
import java.lang.System.err
import java.lang.System.`in`
import javax.security.auth.callback.Callback
import javax.security.auth.callback.CallbackHandler
import javax.security.auth.callback.NameCallback
import javax.security.auth.callback.PasswordCallback
import javax.security.auth.callback.TextOutputCallback
import javax.security.auth.callback.TextOutputCallback.ERROR
import javax.security.auth.callback.TextOutputCallback.INFORMATION
import javax.security.auth.callback.TextOutputCallback.WARNING
import javax.security.auth.callback.UnsupportedCallbackException

internal class IdentityCallbackHandler : CallbackHandler {
  /*
   * Password based encryption (PBE)
   *
   * To create the first hash (Sign up), follow the given steps:
   *   1. Get the password as a char array.
   *   2. Create a salt value.
   *   3. Create a password based encryption key spec.
   *   4. Create a key factory.
   *   5. Generate the hash.
   *   6. Add the iterations and the original salt to your hash.
   */
  override fun handle(callbacks: Array<out Callback>): Unit =
    callbacks.forEach { callback: Callback ->
      when (callback) {
        is TextOutputCallback -> when (callback.messageType) {
          INFORMATION -> println(callback.message)
          WARNING     -> println("WARNING: ${callback.message}")
          ERROR       -> println("ERROR: ${callback.message}")
        }
        is NameCallback -> {
          err.print(callback.prompt)
          err.flush()

          callback.name = BufferedReader(InputStreamReader(`in`)).readLine()
        }
        is PasswordCallback -> {
          err.print(callback.prompt)
          err.flush()

          callback.password = console().readPassword()
        }
        else -> throw UnsupportedCallbackException(callback, "Unsupported Callback Type")
      }
    }
}
