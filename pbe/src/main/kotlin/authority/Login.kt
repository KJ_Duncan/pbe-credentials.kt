package authority

import validity.Validator
import java.util.Arrays
import javax.security.auth.Subject
import javax.security.auth.callback.Callback
import javax.security.auth.callback.CallbackHandler
import javax.security.auth.callback.NameCallback
import javax.security.auth.callback.PasswordCallback
import javax.security.auth.login.LoginException
import javax.security.auth.spi.LoginModule

/* A LoginModule implementation must have a constructor with no arguments.
   This allows classes which load the LoginModule to instantiate it.
   https://docs.oracle.com/en/java/javase/13/docs/api/java.base/javax/security/auth/spi/LoginModule.html */
internal class Login : LoginModule {
  private lateinit var subject: Subject
  private lateinit var callbackHandler: CallbackHandler

  private var sharedState: MutableMap<String, *>? = null
  private var options: MutableMap<String, *>? = null

  private var login: Boolean = false
  private var debug: Boolean = false
  private var boot: Boolean  = false

  /* The authentication process within the LoginModule proceeds in two distinct phases.
      1) The LoginModule's login method gets invoked by the LoginContext's login method. The login method for
         the LoginModule then performs the actual authentication (prompt for and verify a password for example)
         and saves its authentication status as private state information.  Once finished, the LoginModule's
         login method either returns true (if it succeeded) or false (if it should be ignored), or throws a
         LoginException to specify a failure. In the failure case, the LoginModule must not retry the
         authentication or introduce delays.

      2) The LoginContext's overall authentication succeeded (the relevant REQUIRED, REQUISITE, SUFFICIENT and
         OPTIONAL LoginModules succeeded), then the commit method for the LoginModule gets invoked. */
  override fun login(): Boolean {
    val uname: String
    val pword: CharArray
    val valid: Boolean

    val callbacks: Array<Callback> =
      arrayOf<Callback>(NameCallback("Enter user name: "), PasswordCallback("Enter password: ", false))

    // attempts authentication; Conglomerate.Identity(), Conglomerate.Credentials()
    try {
      callbackHandler.handle(callbacks)

      uname = (callbacks[0] as NameCallback).name
      pword = (callbacks[1] as PasswordCallback).password

      (callbacks[0] as NameCallback).name = ""
      (callbacks[1] as PasswordCallback).clearPassword()

      valid = if (boot) Validator.validateBootLogin(uname, pword)
              else Validator.validateUniversalLogin(uname, pword)

      when {
        !valid -> login = false
        else   -> when {
          boot -> TODO("boot derby application with uname and pword, if (true) login = true else false")
          else -> TODO("check derby base table BA_NAME and BA_SECURE_HASH, if (true) login = true else false")
        }
      }

    }
    catch (lex: LoginException) { throw lex } // log and do something
    finally { kotlin.run { pword.clearPassphrase() } }

    /* do user input validation here,
       on invalid input do abort() so
       the object hierarchy's state is
       dropped and clean for next attempt

       (A, B) -> Validator -> Boolean
         where A : String
               B : CharArray

       need an only once function to check if Identity has been instantiated
         Validator.validateBootLogin(uname, pword)
         Validator.validateUniversalLogin(uname, pword)

       call to check authorisation for a derby-db
       loosely couple login to a derby-db as one sysadm many users/projects
       needs to delegate to an interface/abstract in order to handle the correct login has/is a type

       then
       (A, B) -> DerbyDAO -> SQLException
         where A : String
               B : CharArray

       as Derby always produces an SQLException as output,
       the outputs reference 'code' is what we check for conformity

       SQLException -> String -> Boolean

         where Boolean is our final state,
               login success (true) or failure (false)
               return Boolean
       */


    TODO("login")
    return login
  }

  /* The commit method for a LoginModule checks its privately saved state to see if its own authentication
     succeeded. If the overall LoginContext authentication succeeded and the LoginModule's own
     authentication succeeded, then the commit method associates the relevant Principals (authenticated
     identities) and Credentials (authentication data such as cryptographic keys) with the Subject located
     within the LoginModule. */
  override fun commit(): Boolean {
    if (!login) { throw LoginException() }

    // associate: (runtime-token to name-db)
    // domain.DAOFactory.runDAO()
    // run BA_LOGIN trigger TRUE


    // SamplePrincipal implements Principal; a box for, private String name;
    // user/database/name listed in config file, along with UserAction implements PrivilegedAction
    // userPrincipal = new SamplePrincipal(username);
    // if (!subject.getPrincipals().contains(userPrincipal)) { subject.getPrincipals().add(userPrincipal); }

    TODO("commit")
    return true
  }

  /* Logging out a Subject involves only one phase. The LoginContext invokes the LoginModule's logout
     method. The logout method for the LoginModule then performs the logout procedures, such as removing
     Principals or Credentials from the Subject or logging session information. */
  override fun logout(): Boolean {
    TODO("logout")
    login = false
    return true
  }

  override fun initialize(
    subject: Subject,                    /* The Subject represents the Subject currently being authenticated and is updated with relevant Credentials if authentication succeeds. */
    callbackHandler: CallbackHandler,    /* LoginModules use the CallbackHandler to communicate with users. */
    sharedState: MutableMap<String, *>?, /* LoginModules optionally use the shared state to share information or data among themselves. */
    options: MutableMap<String, *>?      /* The LoginModule-specific options represent the options configured for this LoginModule by an administrator or user in the login Configuration (debugging/testing). */
  ) {
    this.subject         = subject
    this.callbackHandler = callbackHandler
    this.sharedState     = null
    this.options         = options

    debug = "true".contentEquals(options?.get("debug").toString())
    // boot the derby application
    boot = "true".contentEquals(options?.get("boot").toString())
  }

  /* If the LoginContext's overall authentication failed (the relevant REQUIRED, REQUISITE, SUFFICIENT and
     OPTIONAL LoginModules did not succeed), then the abort method for each LoginModule gets invoked. In
     this case, the LoginModule removes/destroys any authentication state originally saved. */
  override fun abort(): Boolean {
    if (!login) { throw LoginException() }

    TODO("abort")
    logout()
    return true
  }

  private fun CharArray.clearPassphrase(): Unit = Arrays.fill(this, '\u0000')
}
