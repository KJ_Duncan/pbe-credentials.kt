package domain

// ---------------------------------------------------------------------------------------------------
/* NOTE::Can do more with inline classes; validation, companion objects, operators:
   @see https://github.com/Kotlin/KEEP/blob/master/proposals/inline-classes.md

   problematic: to many call sites to maintain logic in,
                are you solving a problem or creating a
                solution. Planned use-cases only.
// ---------------------------------------------------------------------------------------------------
   A unique runtime hash for each database connection
   here as a possible use-case reminder
   Comparable<String> need not be of type String
   need to also consider the GC on these inliners  */
@JvmInline value class RTToken(private val value: String) : Comparable<RTToken> {
  override fun compareTo(other: RTToken): Int = value.compareTo(other.value)
}

// ---------------------------------------------------------------------------------------------------
@JvmInline value class BAName(val value: String)
@JvmInline value class BAHash(val value: String)
@JvmInline value class BATimeStamp(val value: String)

// ---------------------------------------------------------------------------------------------------
@JvmInline value class SDName(val value: String)
@JvmInline value class SDSecret(val value: String)
@JvmInline value class SDRole(val value: String)
@JvmInline value class SDHash(val value: String)
@JvmInline value class SDAddress(val value: String)
@JvmInline value class SDBinary(val value: ByteArray)
@JvmInline value class SDText(val value: String)
@JvmInline value class SDTimeStamp(val value: String)

// ---------------------------------------------------------------------------------------------------
class BASE private constructor()
class STANDARD private constructor()

// ---------------------------------------------------------------------------------------------------
/* a use-case for: javax.crypto.SealedObject */
data class BaseDTO(val name: BAName,
                   val hash: BAHash,
                   val time: BATimeStamp = BATimeStamp("DEFAULT"))

// ---------------------------------------------------------------------------------------------------
sealed class DTO {  // Transfer Object pattern
  companion object {
    fun init(): StandardDTO {
      return StandardDTO(
        name    = SDName(""),
        secret  = SDSecret(""),
        role    = SDRole(""),
        hash    = SDHash(""),
        address = SDAddress(""),
        binary  = SDBinary(byteArrayOf()),
        text    = SDText(""),
        time    = SDTimeStamp("DEFAULT")
      )
    }
  }
}

/* DTO need to check the unique database name
   for a connection to 'jdbc:derby:<namedb>'
   along with a unique runtime hash for auth
   access to the database.

   first and last types are none null,
   nullable types represent the databases
   nullable columns */
data class StandardDTO(val name: SDName,
                       val secret: SDSecret,
                       val role: SDRole,
                       val hash: SDHash,
                       val address: SDAddress,
                       val binary: SDBinary,
                       val text: SDText,
                       val time: SDTimeStamp = SDTimeStamp("DEFAULT")) : DTO()

// ---------------------------------------------------------------------------------------------------
