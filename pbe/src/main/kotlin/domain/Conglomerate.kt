package domain

import authority.CredentialsCallbackHandler
import authority.IdentityCallbackHandler
import java.lang.System.err
import java.util.UUID
import javax.security.auth.login.LoginContext
import javax.security.auth.login.LoginException

import kotlin.system.exitProcess

/* A sealed class has a private constructor() and can't be instantiated */
internal sealed class Conglomerate {

  /* @author (Vermeulen, M et al. 2020, p.77) */
  fun <A> if2(
    cond: Boolean,
    onTrue: () -> A,
    onFalse: () -> A
  ): A = if (cond) onTrue() else onFalse()

  /* needs a boolean to check against multiple login attempts or tag with an enum class

     Derby handles multi-attempts on a logged in database so just need to check the
     SQLException code as our boolean operation (a last resort) see
     <http://db.apache.org/derby/docs/10.15/devguide/cdevdvlp20458.html>.

     A quick short circuit function, one way is to keep a persistent list
     or map of current active dbNames. More effective/efficient way is to
     check BASE.BA_LOGIN BOOLEAN value. */
  val loggedIn: Boolean = "BA_LOGIN.BOOLEAN" == "TRUE"
  val onlyOnce = if2((loggedIn),
    {
      println("Error: Login attempt unsuccessful...now logging the details")
      println("Placeholder for logging a message")
    },
    { println("Some polite and respectable welcome message") })


  object Identity : Conglomerate() {
    private val uuid: UUID = UUID.randomUUID()

    override fun toString(): String = "Identity($uuid)"

    operator fun invoke() {
      var attempts: Int = 0
      val context: LoginContext

      /* -Djava.security.auth.login.config==pbe-credentials.config
            Identity { Login required; }; */
      try { context = LoginContext("Identity", IdentityCallbackHandler()) }
      catch (lex: LoginException) { err.println("Error on login"); exitProcess(1) }
      catch (sex: SecurityException) { err.println("Error on login"); exitProcess(1) }

      do {
        // attempt authentication
        try { context.login(); break }
        catch (lex: LoginException) { err.println("Error on login") }
        finally { attempts++ }
      } while (attempts < 3)

      if2((attempts >= 3),
        { println("Error on Login maximum attempts reached...")
          exitProcess(1) },
        { println("Login Successful") })
    }
  }

  /* Providing explicit implementations for the componentN() and copy() functions is not allowed.
     To exclude a property from the generated implementations, declare it inside the class body.
     data class implements; componentN(), toString(), equals(), hashCode(), and copy().
     https://kotlinlang.org/docs/reference/data-classes.html

     @see Mark Slater, sealed class User, https://youtrack.jetbrains.com/issue/KT-11914#focus=Comments-27-4244038.0-0
     @see Oracle, Value-based Classes, https://docs.oracle.com/javase/8/docs/api/java/lang/doc-files/ValueBased.html

     data class private constructor()
       companion object

     dependency injection is a possibility:
       private fun Credentials.Companion.function() = object : Typeclass<Credentials> { }

   */
  data class Credentials(private val uuid: UUID = UUID.randomUUID()) : Conglomerate() {

    private val lazyDB: Lazy<DAOFactory> = lazy { DAOFactory.runDAO<STANDARD>(DAOFactory.STANDARD, "database name") }

    override fun toString(): String = "Credentials($uuid)"

    operator fun invoke() {
      var attempts: Int = 0
      val context: LoginContext

      /* -Djava.security.auth.login.config==pbe-credentials.config
            Credentials { Login required; }; */
      try { context = LoginContext("Credentials", CredentialsCallbackHandler()) }
      catch (lex: LoginException) { err.println("Error on login"); throw lex } // return error and log as their are other users
      catch (sex: SecurityException) { err.println("Error on login"); throw sex } // return error and log as their are other users

      do {
        // attempt authentication
        try { context.login(); break }
        catch (lex: LoginException) { err.println("Error on login") }
        finally { attempts++ }
      } while (attempts < 3)

      if2((attempts >= 3),
        { println("Error on Login maximum attempts reached...") },
        { println("Login Successful") })
    }
  }

  // used for dependency injection
  /* TEST::Conglomerate.Companion.object with an internal modifier */
  companion object
}
