package validity

import kotlin.math.abs

internal object CheckCredentials {

  private val isEven: (String) -> Boolean = { it.length % 2 == 0 }

  /* Parse 2 characters at a time and convert from base 16 to base 2 */
  private fun String.fromHex(): ByteArray {
    check(isEven(this)) { "hexadecimal string needs to be an even-length: ${this.length}" }

    val size   = this.length / 2
    val binary = ByteArray(size)

    (binary.indices).forEach { i: Int ->
      binary[i] = Integer.parseInt(this.substring(2 * i, 2 * i + 2), 16).toByte()
    }

    return binary
  }

  /* Authentication string and Password Based Encryption
     implementations referenced below.

     <algorithm>:<iterations>:<base64(salt)>:<base64(hash)>

     Where:

      * algorithm    -- the algorithm used to generate the hash
      * iterations   -- the number of iterations used to generate the hash
      * base64(salt) -- the salt used to generate the hash, base64-encoded
      * base64(hash) -- the hash value, base64-encoded

     Can switch to return hash.contentEquals(testHash) if
     slowEquals length constant time comparison is undesired.

     SHA256 is a cryptographic hash function that produces a
     256-bit hash value (64 characters in length) where 1 byte
     equals 8 bits therefore 32 * 8 = 256 bits

     Internet Engineering Task Force (IETF) 2017, PKCS #5: Password-Based Cryptography Specification,
     Version 2.1, viewed 23 June 2020, https://tools.ietf.org/rfc/rfc8018.txt

     Jakarta Security API v2.0.0-RC2, Interface Pbkdf2PasswordHash,
     jakarta.security.enterprise.identitystore, viewed 10 August 2020, https://javadoc.io/doc/jakarta.security.enterprise/jakarta.security.enterprise-api/latest/jakarta/security/enterprise/identitystore/Pbkdf2PasswordHash.html */
  private fun CharArray.isValid(authentication: String): Boolean {
    val params = authentication.split(":")
    val algorithm  = params[0]  // "PBKDF2WithHmacSHA256"
    val iterations = params[1].toInt()
    val salt = params[2].fromHex()
    val hash = params[3].fromHex()

    val spec = javax.crypto.spec.PBEKeySpec(this, salt, iterations, 32 * 8)
    val skf  = javax.crypto.SecretKeyFactory.getInstance(algorithm)
    val testHash: ByteArray = skf.generateSecret(spec).encoded

    return slowEquals(hash, testHash)
  }

  /**
   * The difference between the length of the byte array 'hash'
   * and the length of the byte array 'testHash', if they are equal
   * this will give a zero value.
   *
   * @author Mayoral, F 2013, Instant Java Password and Authentication Security,
   *         Packt Publishing, viewed on 08 June 2020,
   *         https://www.packtpub.com/application-development/instant-java-password-and-authentication-security-instant
   */
  private fun slowEquals(hash: ByteArray, testHash: ByteArray): Boolean {
    /* ByteArray.size xor ByteArray.size
                  Int xor Int
       hash.size xor testHash.size should be 0,
       representing equivalence in length. */
    var diff = hash.size xor testHash.size
    var i = 0

    /* Will always be short on the longest length,
       the aim is to slow the process down to
       obfuscate brute force and dictionary attacks */
    while (i < hash.size && i < testHash.size) {
      /* Inclusive assignment: if diff == 0,
         assigns to diff the result of comparing two bytes,
         if they are equal it assigns a 0, else 1
         00, 01, 10, 11
         ^^----------^^ == 0
         ----^^--^^---- == 1

         Additional function calls slow the process down
         when comparing the left bit with the right bit.

         Byte.valueOf(x).compareTo(Byte.valueOf(y))
         if x == y then 0, if x < y then -1, if x > y then 1

         number of function calls in the assignment
        ---1---2-------3-----4---------------5--- */
      diff += abs(hash[i].compareTo(testHash[i]))
      i++
    }

    return diff == 0
  }

  operator fun invoke(passphrase: CharArray, authentication: String): Boolean { return passphrase.isValid(authentication) }
}
