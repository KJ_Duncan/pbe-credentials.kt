package validity

internal object SecureHash {
  private val logger = java.util.logging.Logger.getLogger(SecureHash::class.java.name)

  /* Mathematically, an iteration count of 'c' will increase the security strength of a
     password by log2(c) bits against trial-based attacks like brute force or dictionary
     attacks. [...] A minimum iteration count of 1,000 is recommended (IETF 2017, s4.2). */
  private const val iterations: Int = 4096
  private const val algorithm: String = "PBKDF2WithHmacSHA256"

  /* Base 2 to base 16 conversion with padding */
  private fun ByteArray.toHex(): String {
    val bi  = java.math.BigInteger(1, this)
    val hex = bi.toString(16)
    val paddingLength = (this.size * 2) - hex.length

    return when {
      paddingLength > 0 -> "${String.format("%0${paddingLength}d", 0)}$hex"
      else -> hex
    }
  }

  /* A salt of 32 bytes, 8 bits to 1 byte: 8 * 32 = 256 bit salt strength.

     If the salt is 64 bits long, for instance, there will be as many as 2^64 keys for
     each password. Again, if the salt is 64 bits long, the chance of "collision"
     between keys does not become significant until about 2^32 keys have been produced,
     according to the Birthday Paradox. Recommended for salt selection [...] should be
     at least eight octets (64 bits) long (IETF 2017, s4.1). */
  private fun createSalt(): ByteArray {
    val random: java.security.SecureRandom = java.security.SecureRandom()
    val salt = ByteArray(64)
    random.nextBytes(salt)

    return salt
  }

  /* Hash database storage formatted as "algorithm:iterations:salt:hash"

     A key derivation function produces a derived key from a base key and
     other parameters. In a password-based key derivation function, the
     base key is a password, and the other parameters are a salt value and
     an iteration count [...] PBKDF2 is recommended for new applications
     (IETF 2017, s5). */
  private fun CharArray.generateHash(): String {
    val salt = createSalt()
    val spec = javax.crypto.spec.PBEKeySpec(this, salt, iterations, 32 * 8)
    val skf  = javax.crypto.SecretKeyFactory.getInstance(algorithm)
    val hash = skf.generateSecret(spec).encoded

    return "$algorithm:$iterations:${salt.toHex()}:${hash.toHex()}"
  }

  operator fun invoke(passphrase: CharArray): String { return passphrase.generateHash() }
}
