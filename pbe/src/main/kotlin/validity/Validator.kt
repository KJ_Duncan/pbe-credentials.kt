package validity

internal object Validator {
  /*
   * Validation could keep a running total though problematic as AAA-bbb@111!
   * would pass scrutiny. Is this a responsibility for user or developer? Due
   * to information asymmetry the programmer has a fiduciary duty to the user.
   * Therefore a software developer should enforce industry best practice.
   *
   * check()   throws IllegalStateException
   * require() throws IllegalArgumentException
   *
   * (A, B) -> Validator -> Boolean
   *   where A : String
   *         B : CharArray
   *
   */
  private val MINIMUM_LENGTH: Int = 8
  private val MAXIMUM_LENGTH: Int = 42

  fun validateBootLogin(username: String, passphrase: CharArray): Boolean =
    validateStringInput(username) && validateBootInput(passphrase)

  fun validateUniversalLogin(username: String, passphrase: CharArray): Boolean =
    validateStringInput(username) && validateCharArrayInput(passphrase)

  private fun validateStringInput(input: String): Boolean =
    try {
      check(input.isNotEmpty())
      check(input.isInValidRange())
      require(input.all { it.isAlphaNumeric() })
      require(minimumStandardUsernameValidation(input))
      true
    } catch (e: Exception) { false }

  /* Derby boot password is an alphanumeric string at least eight characters long */
  private fun validateBootInput(input: CharArray): Boolean =
    try {
      check(input.isNotEmpty())
      check(input.isInValidRange())
      require(input.all { it.isAlphaNumeric() })
      val sb = StringBuilder().append(input)
      /* TEST::validateBootInput stringBuilder clears input on exception thrown, else use the finally block */
      require(sb.matches(bootPasswordValidation))
        .also { sb.clear() }
      true
    } catch (e: Exception) { false }
    // finally { sb.clear() }

  /* NOTE::validateInput https://github.com/JetBrains/kotlin/blob/master/libraries/stdlib/samples/test/samples/text/char.kt
                         https://docs.oracle.com/en/java/javase/13/docs/api/java.base/java/lang/Character.html */
  private fun validateCharArrayInput(input: CharArray): Boolean = /* if input invalid call validateAttempts else continue */
    try {
      check(input.isNotEmpty())
      check(input.isInValidRange())
      check(input.any { it.isIdentifierIgnorable() })
      require(input.none { it.noneOfProhibitedChars() })
      /* TEST::validateCharArrayInput stringBuilder pattern does return a string, check were it is held (memory/heap/string-pool) */
      buildString {
        append(input)
        require(matches(bootPasswordValidation))
          .also { clear() }
      }
      true
    } catch (e: Exception) { false }

  /* TODO::contracts https://github.com/JetBrains/kotlin/blob/master/libraries/stdlib/samples/test/samples/contracts/contracts.kt
     name of the database cannot contain a semicolon ':'
     bootPassword is an alphanumeric string at least eight characters long

     encryptionKey must be a hexadecimal string at least 16 digits in length (8 bytes),
     and must contain an even number of digits. A character involved in a bit operation
     is converted to an integer. see Base64.kt

     TEST::bootPasswordValidation consecutive chars length 8, 44 check derby-db requirements */
  private val bootPasswordValidation: Regex =
    """^()$""".toRegex()

  /* TEST::usernameValidation consecutive chars length 8, ?? will be used as database name check derby-db requirements */
  private val usernameValidation: Regex =
    """^([a-zA-Z][a-zA-Z0-9_]{8,})$""".toRegex()

  /* As at 15/06/2020 https://regex101.com/r/SYKiS0/1 */
  private val passwordValidation: Regex =
    """^(?:(?=.*\d)(?=.*[A-Z])(?=.*[a-z])|(?=.*\d)(?=.*[^A-Za-z0-9])(?=.*[a-z])|(?=.*[^A-Za-z0-9])(?=.*[A-Z])(?=.*[a-z])|(?=.*\d)(?=.*[A-Z])(?=.*[^A-Za-z0-9]))(?!.*(.)\1{2,})[A-Za-z0-9!~<>,;:_=?*+#.\"&§%°()\|\[\]\-\$\^\@\/'\{\}\\`]{13,43}$""".toRegex()

  /* TODO::prohibitedList checklist of characters and codes, unicode translations? \\s = [ \t\n\r\f] and \\S = [^ \t\n\r\f] */
  private val prohibitedUnicodeList: List<Char> =
    listOf<Char>('\u003A', '\u003F', '\u000A', '\u000D', '\u0020', '\u0009', '\u0008', '\u000C', '\u0027', '\u0022', '\u005C', '\u003D', '\u0021', '\u002A')

  private val prohibitedCharacterList: List<Char> =
    listOf<Char>(':', '?', '\n', '\r', ' ', '\t', '\b', '\'', '"', '\\', '=', '!', '*')

  /* TODO::minimumStandard insure no prohibited chars for derby-db and SQL injection attacks, then raise to industry best practices */
  private fun minimumStandardUsernameValidation(str: String): Boolean     = str.matches(usernameValidation)
  private fun minimumStandardPasswordValidation(str: String): Boolean     = str.matches(passwordValidation)
  private fun minimumStandardBootPasswordValidation(str: String): Boolean = str.matches(bootPasswordValidation)

  /* NOTE: https://github.com/JetBrains/kotlin/blob/master/libraries/stdlib/test/text/StringTest.kt#L43 */
  private fun Char.isAsciiDigit(): Boolean   = this in '0'..'9'
  private fun Char.isAsciiLetter(): Boolean  = this in 'A'..'Z' || this in 'a'..'z'
  private fun Char.isAlphaNumeric(): Boolean = this.isAsciiDigit() || this.isAsciiLetter()

  private fun Char.noneOfProhibitedChars(): Boolean   = this !in prohibitedCharacterList
  private fun Char.noneOfProhibitedUnicode(): Boolean = this !in prohibitedUnicodeList

  private fun String.isInValidRange(): Boolean    = this.length in 13..43
  private fun CharArray.isInValidRange(): Boolean = this.size in 13..43

  /* NOTE: when things get crazy don't get lazy
       https://github.com/javabeanz/owasp-security-logging/blob/master/owasp-security-logging-common/src/main/java/org/owasp/security/logging/Utils.java#L76 */
  private fun String.replaceCRLFWithUnderscore(): String = this.replace('\n', '_').replace('\r', '_')
}
