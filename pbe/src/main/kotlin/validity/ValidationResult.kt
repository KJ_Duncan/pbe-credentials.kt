package validity

/* https://javadoc.io/static/jakarta.security.enterprise/jakarta.security.enterprise-api/2.0.0-RC2/jakarta/security/enterprise/identitystore/CredentialValidationResult.Status.html
   https://javadoc.io/static/jakarta.security.enterprise/jakarta.security.enterprise-api/2.0.0-RC2/jakarta/security/enterprise/identitystore/CredentialValidationResult.html */
private enum class ValidationResult { INVALID, NOT_VALIDATED, VALID; }

