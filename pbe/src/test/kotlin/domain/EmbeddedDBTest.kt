package domain

import domain.EmbeddedDB
import io.kotest.core.spec.style.StringSpec

internal class EmbeddedDBTest : StringSpec() {

  private val embeddedDB: EmbeddedDB = EmbeddedDB()
  /*

Testing the derby embedded database

---
DOING

1) visualise the interactions with a uml design

2) then pre-conditions and post-conditions

3) write them out then test them out

---
TODO

test the existence of the class (entry)

test inner class IdentityConnectionURL
  constructor arguments
    username: String,  passphrase: CharArray

test IdentityConnectionURL returns type Connection

test Connection attributes are correct and expected

test IdentityConnectionURL passphrase and _passphrase
  pre-conditions are:
  post-conditions are:


test IdentityConnectionURL throws an SQLException on variables

test IdentityConnectionURL visibility

---
TODO

test authentication and authorisation

test garbage collection

test encryption

---
TODO

test coroutines

test flows

  */

  init {
    "First test" {
      // goes here
    }
  }
}
