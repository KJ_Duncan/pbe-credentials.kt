package manager

import io.kotest.assertions.throwables.shouldNotThrow
import io.kotest.assertions.throwables.shouldNotThrowExactly
import io.kotest.assertions.throwables.shouldThrow
import io.kotest.assertions.throwables.shouldThrowExactly
import io.kotest.assertions.throwables.shouldThrowMessage
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.nulls.shouldNotBeNull
import io.kotest.matchers.shouldBe
import io.kotest.matchers.string.shouldBeEqualIgnoringCase
import io.kotest.matchers.string.shouldMatch
import io.kotest.matchers.types.shouldBeInstanceOf
import io.kotest.matchers.types.shouldBeTypeOf
import java.sql.SQLException

typealias A8 = (String) -> (String) -> (String) -> (String) -> (String) -> (ByteArray) -> (String) -> (String) -> String

internal class ResultTCTest : StringSpec({

  val arity8fn: A8 =
    { a -> { b -> { c -> { d -> { e -> { f -> { g -> { h -> "$a+$b+$c+$d+$e+${f[0]}+$g+$h = ${a.toInt()+b.toInt()+c.toInt()+d.toInt()+e.toInt()+f[0]+g.toInt()+h.toInt()}" } } } } } } } }

  fun map8() =
    pureResult<A8, Exception>(arity8fn) ap Success("1") ap Success("2") ap Success("3") ap Success("4") ap Success("5") ap Success(byteArrayOf(0b00000110)) ap Success("7") ap Success("8")

  fun map8Failure() =
    pureResult<A8, Exception>(arity8fn) ap Success("1") ap Success("2") ap Success("hello") ap Success("4") ap Success("5") ap Success(byteArrayOf(0x6)) ap Success("7") ap Success("8")

  "map8 should be of type Result.Success<String>" {
    map8().shouldBeTypeOf<Result.Success<String>>()
  }

  "map8 should equal Success(value=1+2+3+4+5+6+7+8 = 36)" {
    map8() shouldBe Success(value = "1+2+3+4+5+6+7+8 = 36")
  }

  /* TEST:: an applicative applies a function in context to a value in context,
     so if an uncaught exception occurs the function would need to be applied
     to a raw value out of context. Applicatives work over functions that
     maintain referential transparency.

     map8Failure return type is Result<String, A8>

     Result<S, E>.getOrElse(default: () -> S): S */
  "FIXME, map8Failure should be of type Result.Failure<NumberFormatException>" {
    shouldThrowMessage("For input string: \"hello\"") { map8Failure() }
  }

  // Observation: Never checks the it.message case, always throws exception first, unless wrapped try/catch block then checks the test case
  // Result<S, E>.orElse(option: () -> Result<S, E>): Result<S, E>
  "FIXME, map8Failure should not throw NumberFormatException" {
    resultFrom { map8Failure().forEach(onFailure = { it.message.shouldBeEqualIgnoringCase("For input string: \"hello\"") }) }
  }

  /* TEST::Start here Result bad behaviour, why? Definitely forgot a step in sealed class.
     resultFrom { Wrapper } not the solution, I have the base case in flatMap, so next is debugger.
     Check order of operations and infix precedence. Use example.Result to validate TC

     Observation: flatMap wrapping return value is Failure -> Failure(it.reason) or Failure(it.reason).fix(), again always throws exception.
     Observation: Success(wrapper around the below function), again always throws exception
     Observation: Failure(wrapper around the below function), again always throws exception */
  "FIXME, when Failure should not throw NumberFormatException" {
    // (pureResult<(String) -> Int, Throwable>(String::toInt) ap Result.Success("hello")).forEach(onFailure = { it })
    shouldNotThrow<NumberFormatException> {
      resultFrom { pureResult<(String) -> Int, Throwable>(String::toInt) ap Result.Success("hello") }
    }
  }

// ---------------------------------------------------------------------------------------------------

  val success = Result.Success(value = "true")
  val failure = Result.Failure(reason = Exception("false"))
  val sqlexce = Result.SQLState(code = SQLException("42500"))

  "forEach success, failure, and sqlState their message should match true, false, 42500" {
    success.forEach(onSuccess = { it.shouldMatch("true") })
    failure.forEach(onFailure = { it.message.shouldMatch("false") })
    sqlexce.forEach(onSqlState = { it.message.shouldMatch("42500") })
  }

  "assert Success, Failure, and SQLState are not null"() {
    success.shouldNotBeNull()
    failure.shouldNotBeNull()
    sqlexce.shouldNotBeNull()
  }

  "Success value equals true"() {
    success.value shouldBe "true"
  }

  "Failure reason is an Exception"() {
    shouldThrowExactly<Exception> { throw failure.reason }
  }

  "SQLState type should be SQLException" {
    sqlexce.code.shouldBeTypeOf<SQLException>()
  }

  "Failure reason message is false"() {
    failure.reason.message shouldBeEqualIgnoringCase "false"
  }

  "SQLState type SQLException should have message 42500" {
    sqlexce.code.message shouldBeEqualIgnoringCase "42500"
  }

  "resultFrom should not throw Exception"() {
    shouldNotThrow<Exception> { resultFrom { Exception() } }
  }

  "forEach onFailure should not throw a ArithmeticException on 1 div 0"() {
    shouldNotThrowExactly<ArithmeticException> {
      resultFrom { (1 / 0) }.forEach(onFailure = { it.cause })
    }
  }
})


/*
typealias IO = Unit
val arityOf8fn: (SDName) -> (SDSecret) -> (SDRole) -> (SDHash) -> (SDAddress) -> (SDBinary) -> (SDText) -> (SDTimeStamp) -> IO = { { { { { { { { it } } } } } } } }
pure(arityOf8fn) ap Success(SDName("1")) ap Success(SDSecret("2")) ap Success(SDRole("3")) ap Success(SDHash("4")) ap Success(SDAddress("5")) ap Success(SDBinary(byteArrayOf(0x6))) ap Success(SDText("7")) ap Success(SDTimeStamp("8"))
 */
