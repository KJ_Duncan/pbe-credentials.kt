package manager

import io.kotest.core.spec.style.StringSpec

internal class ParserTest : StringSpec() {

  init {
    "first test" {
      // goes here
    }
  }
}

/*
PROPERTY BASED TESTING

abstract class Laws : Parsers<ParseError> {
  private fun <A> equal(
    p1: Parser<A>,
    p2: Parser<A>,
    i: Gen<String>
    ): Prop =
      forAll(i) { s -> run(p1, s) == run(p2, s) }

  fun <A> mapLaw(p: Parser<A>, i: Gen<String>): Prop =
    equal(p, p.map { a -> a }, i)
}
*/
