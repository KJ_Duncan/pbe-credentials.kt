package manager

import domain.SDAddress
import domain.SDBinary
import domain.SDHash
import domain.SDName
import domain.SDRole
import domain.SDSecret
import domain.SDText
import domain.SDTimeStamp
import domain.StandardDTO
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.comparables.shouldBeEqualComparingTo
import io.kotest.matchers.types.shouldBeInstanceOf
import manager.Lens.Companion.compose

internal class LensTest : StringSpec({

  /* TEST::standardDTO setup a factory and property based tests */
  val standardDTO = StandardDTO(
    name    = SDName("name"),
    secret  = SDSecret(""),
    role    = SDRole(""),
    hash    = SDHash(""),
    address = SDAddress(""),
    binary  = SDBinary(byteArrayOf()),
    text    = SDText(""),
    time    = SDTimeStamp("DEFAULT")
  )

  val standardDTOLens = object : StandardDTOLenses {}

  "StandardDTOLens get method should return name" {
    standardDTOLens.run {
      nameLens().get()(standardDTO) shouldBeEqualComparingTo "name"
    }
  }

  "StandardDTOLens set method name=next should be type StandardDTO" {
    standardDTOLens.run {
      nameLens().set()(standardDTO, "next").shouldBeInstanceOf<StandardDTO>()
    }
  }

  "StandardDTOLens get method should return next" {
    standardDTOLens.run {
      val k = nameLens().set()(standardDTO, "next")
      nameLens().get()(k) shouldBeEqualComparingTo "next"
    }
  }

  "Lens compose method should return".config(enabled = false) {
    // Lens<StandardDTO, StandardDTO>
    // Lens<StandardDTO, String>

    // val g = nameLens()
    // val h = lensTime()

    // O to I -> I to V
    // StandardDTO to StandardDTO -> StandardDTO to String
    // val k = compose(h, g)
  }

})
