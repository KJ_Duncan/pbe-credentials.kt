package manager

import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.comparables.shouldBeEqualComparingTo
import io.kotest.matchers.ints.shouldBeExactly
import io.kotest.matchers.nulls.shouldNotBeNull
import io.kotest.matchers.types.shouldBeInstanceOf

internal class StateTest : StringSpec() {
  // (S) -> Pair<S, T>
  private val state: State<Int, Int> =
    { s -> Pair(s, s * s) }

  init {
    "State should not be null" {
      state.shouldNotBeNull()
    }

    "pure<String, Int>(5) should be of instance State<String, Int>" {
      pureState<String, Int>(5)
        .shouldBeInstanceOf<State<String, Int>>()
    }

    // State<S, T>.map(fn: (T) -> R)
    "State<Int, Int> map (Int) -> Float should be 9.0" {
      val g = state.map { i -> i.toFloat() }
      val p = g.invoke(3)

      p.first.shouldBeExactly(3)
      p.second.shouldBeEqualComparingTo(9F)
    }

    // State<S, T>.flatMap(fn: (T) -> State<S, R>)
    "State<Int, Int> flatMap 11 should be pure(121.toString())" {
      val g = state.flatMap { i -> pureState<Int, String>(i.toString()) }
      val p = g.invoke(11)

      p.first.shouldBeExactly(11)
      p.second.shouldBeEqualComparingTo("121")
    }

    // State<S, (T) -> R>.ap(st: State<S, T>)
    "State<String, (String) -> Int> ap State<String, Int> should be (9.toString() to 81)" {
      val A: State<String, (String) -> Int> =
        { s -> s to { t -> (t.toInt() * t.toInt()) } }

      val B: State<String, String> =
        { s -> s to s }

      val h = A.ap { s -> B.invoke(s) }
      val p = h.invoke("9")

      p.first.shouldBeEqualComparingTo("9")
      p.second.shouldBeExactly(81)
    }

    // lift(fn: (T) -> R): (State<S, T>) -> State<S, R>
    // State<S, T> -> State<S, T>.map((T) -> R)
    "lift<Int, Int, Int> State.invoke(5) should be (5 to 625)" {
      val h = liftState<Int, Int, Int> { i -> state.invoke(i).second } // i to (i * i)
      val g = h.invoke { i -> state.invoke(i) }                        // i to (i * (i * i))
      val p = g.invoke(5)                                              // i to (i * (i * (i * i)))

      p.first.shouldBeExactly(5)                                       // i
      p.second.shouldBeExactly(625)                                    // (5 * 5 * 5 * 5)
    }
  }
}
