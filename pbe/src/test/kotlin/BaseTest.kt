import java.sql.Timestamp
import java.time.LocalDateTime
import java.time.Month

/* An annotation or configuration hierarchy
   in build.gradle.kts

     useJUnitPlatform { excludeTags("tagName") }

     val testTaskName by registering(Test::class) {
       useJUnitPlatform { includeTags("tagName") }
     }

   on console
     $ ./gradlew testTaskName --rerun-tasks (flag ignores any cache)
 */
@Suppress("DIVISION_BY_ZERO")
interface BaseTest


/*
 * This gives you the automated hashCode, equals and toString,
 * and eliminates the copy method as desired, however:
 *  - You lose the ability to destruct instances of the class Name (correct evaluation: val (hash, datum) = User.of(Hash, Datum) will not compile)
 *  - The toString outputs the type as UserData rather than User (correct evaluation: User(empty, empty) == User$Companion@hash)
 *  - The intention of the code isn't entirely clear (well documented code always solves this problem)
 *
 * Reason: The copy method on a data class circumvents a private constructor,
 *         The kotlin compiler generates unproductive boilerplate code,
 *         to keep a developer moving forward.
 *
 * @author Slater, M 2020, Confusing data class copy with private constructor,
 *         Youtrack Jetbrains, viewed 07 July 2020,
 *         https://youtrack.jetbrains.com/issue/KT-11914#focus=Comments-27-4244038.0-0
 */
internal inline class Hash(val value: String)
internal inline class Datum(val value: String)

internal sealed class User {

  abstract val hash: Hash
  abstract val datum: Datum

  private data class UserData(override val hash: Hash, override val datum: Datum) : User()

  override fun toString(): String = "User(empty, empty)"

  companion object {
    fun of(hash: Hash, datum: Datum): User = UserData(hash, datum)
  }
}

/* Lazy initializers for current time */
private class LazyTimestamp(val initializer: () -> Timestamp) {
  val time: Timestamp
    get() = initializer()
}

private val timestamp: LazyTimestamp = LazyTimestamp({ java.sql.Timestamp.valueOf(LocalDateTime.now()) })
private val now: () -> Timestamp = { java.sql.Timestamp.valueOf(LocalDateTime.now()) }

private val y2k: Timestamp = java.sql.Timestamp.valueOf(LocalDateTime.of(1999,12,31,23,59))
private val ky2: Timestamp = java.sql.Timestamp.valueOf(LocalDateTime.of(2000,1,1,0,1))

/*
 * Generalise database 'expected' should match 'actual', where actual is a derby database:
 * BASE = [pk, name, hash, time, login]
 * STANDARD = [pk, name, secret, role, hash, address, binary, text, time]
 *
 * @see java.sql.JDBCType, generic SQL types, https://docs.oracle.com/en/java/javase/13/docs/api/java.sql/java/sql/JDBCType.html
 *      java.net.URI, URI syntax and components, https://docs.oracle.com/en/java/javase/13/docs/api/java.base/java/net/URI.html
 *      RFC 3986, Security Considerations, URI Generic Syntax, section 7, https://tools.ietf.org/html/rfc3986#section-7
 */
internal val BASE_DB: Array<Array<out Any>> = arrayOf(
  arrayOf(1, "NAME", "ALGORITHM:ITERATIONS:SALT:HASH", y2k, false),
  arrayOf(2, "NAME", "ALGORITHM:ITERATIONS:SALT:HASH", ky2, true),
  arrayOf(3, "NAME", "ALGORITHM:ITERATIONS:SALT:HASH", now(), true)
)

/* Lazy init of datastructures, curiosity regarding timestamp initialisation */
internal val STANDARD_DB: () -> Array<Array<out Any>> = {
  arrayOf(
    arrayOf(1, "NAME", "HASH", y2k),
    arrayOf(2, "NAME", "HASH", ky2),
    arrayOf(3, "NAME", "HASH", timestamp.time)
  )
}
