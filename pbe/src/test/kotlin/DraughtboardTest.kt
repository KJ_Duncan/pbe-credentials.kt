import domain.DTO
import domain.SDName
import domain.StandardDTO
import io.kotest.core.spec.style.StringSpec
import manager.Result
import manager.StandardDTOLenses
import manager.State
import manager.resultFrom
import java.util.Locale

typealias StateSetterResult<T> = State<StandardDTO, (T, StandardDTOLenses) -> Result<StandardDTO, Throwable>>
typealias StateGetterResult<T> = State<StandardDTO, (StandardDTOLenses) -> Result<T, Throwable>>

typealias StateSetter<T> = State<StandardDTO, (T, StandardDTOLenses) -> StandardDTO>
typealias StateGetter<T> = State<StandardDTO, (StandardDTOLenses) -> T>


@OptIn(ExperimentalStdlibApi::class)
internal class DraughtboardTest : StringSpec() {
  init {
    "rough thoughts/drafts".config(enabled = false) {
      val std = DTO.init()
      val stdLens = object : StandardDTOLenses {}
/* ---------------------------------------------------------------------------------------------------
   GOAL: Standardised
         Encrypted
         Databases (1..n)

   Standardised IO
   ---------------------------------------------------------------------------------------------------
   (Parse users input) to perform (Derby IO) and catch the resulting (SQLException code)
   (Parse input) wrap (Derby IO) to (catch SQLException)
   ---------------------------------------------------------------------------------------------------
   State Monad gets back the original state and the computation (a round trip)
   ---------------------------------------------------------------------------------------------------
   Need a Parser is (String or ByteArray) and length/size in min..max
   typealias Parser<S> = (String) -> Result<S, ParseError>

   @author Hutton, G 2016, 13. Monadic parsing, Programming in Haskell, second ed.
   @author Vermeulen, M et al. 2020, Parser combinators, p.172

   Need a handle to Derby IO (OperationsDAO)
   ---------------------------------------------------------------------------------------------------
   IO -> (Result -> (State -> OperationsDAO)) -> (Result -> IO)
   --------------------------------------------------------------------------------------------------- */

      val stateSetNameResult: StateSetterResult<String> =
        { dto -> dto to { s, t -> resultFrom { t.run { nameLens().set()(dto, s) } } } }

      val stateGetNameResult: StateGetterResult<String> =
        { dto -> dto to { t -> resultFrom { t.run { nameLens().get()(dto) } } } }

// ---------------------------------------------------------------------------------------------------

      stateSetNameResult(DTO.init())
        .second("Awesome", object : StandardDTOLenses {})
        .forEach(onSqlState = { it.errorCode })

      stateGetNameResult(DTO.init())
        .second(object : StandardDTOLenses {})
        .forEach(onSuccess = { it.capitalize(locale = Locale.FRENCH) },
                 onFailure = { it.localizedMessage },
                 onSqlState = { it.sqlState })

// --- Template Setter -------------------------------------------------------------------------------

      val stateSetName: StateSetter<String> =
        { dto -> dto to { s, t -> t.run { nameLens().set()(dto, s) } } }

      val stateSetSecrete: StateSetter<String> =
        { dto -> dto to { s, t -> t.run { secretLens().set()(dto, s) } } }

      val stateSetRole: StateSetter<String> =
        { dto -> dto to { s, t -> t.run { roleLens().set()(dto, s) } } }

      val stateSetHash: StateSetter<String> =
        { dto -> dto to { s, t -> t.run { hashLens().set()(dto, s) } } }

      val stateSetAddress: StateSetter<String> =
        { dto -> dto to { s, t -> t.run { addressLens().set()(dto, s) } } }

      val stateSetBinary: StateSetter<ByteArray> =
        { dto -> dto to { ba, t -> t.run { binaryLens().set()(dto, ba) } } }

      val stateSetText: StateSetter<String> =
        { dto -> dto to { s, t -> t.run { textLens().set()(dto, s) } } }

      val stateSetTime: StateSetter<String> =
        { dto -> dto to { s, t -> t.run { timeLens().set()(dto, s) } } }

// --- Template Getter -------------------------------------------------------------------------------

      val stateGetName: StateGetter<String> =
        { dto -> dto to { t -> t.run { nameLens().get()(dto) } } }

      val stateGetSecrete: StateGetter<String> =
        { dto -> dto to { t -> t.run { secretLens().get()(dto) } } }

      val stateGetRole: StateGetter<String> =
        { dto -> dto to { t -> t.run { roleLens().get()(dto) } } }

      val stateGetHash: StateGetter<String> =
        { dto -> dto to { t -> t.run { hashLens().get()(dto) } } }

      val stateGetAddress: StateGetter<String> =
        { dto -> dto to { t -> t.run { addressLens().get()(dto) } } }

      val stateGetBinary: StateGetter<ByteArray> =
        { dto -> dto to { t -> t.run { binaryLens().get()(dto) } } }

      val stateGetText: StateGetter<String> =
        { dto -> dto to { t -> t.run { textLens().get()(dto) } } }

      val stateGetTime: StateGetter<String> =
        { dto -> dto to { t -> t.run { timeLens().get()(dto) } } }

// ---------------------------------------------------------------------------------------------------

      val sdto = stateSetName(std).second("newName", stdLens)

      val stateO2: State<StandardDTO, SDName> =
        { dto -> dto to dto.name }

      val state03: State<StandardDTO, (SDName) -> String> =
        { dto -> dto to { t -> t.value } }

    }
  }
}
