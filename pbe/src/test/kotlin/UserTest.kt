import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.nulls.shouldNotBeNull
import io.kotest.matchers.shouldBe
import io.kotest.matchers.shouldNotBe
import io.kotest.matchers.types.shouldBeInstanceOf

internal class UserTest : StringSpec({

  val hash = Hash("code")
  val datum = Datum("contents")

  "User should not be null" {
    User.shouldNotBeNull()
  }

  "User of should be instance of User" {
    User.of(hash = hash, datum = datum).shouldBeInstanceOf<User>()
  }

  "User of copy method should not compile, uncomment test to see failure" {
    // User.of(hash = hash, datum = datum).copy(hash = Hash("after"))
  }

  "User of value deconstruction should not compile, uncomment test to see failure" {
    // val (hash, datum) = User.of(hash = hash, datum = datum)
  }

  "User of hash should be hash.value = code" {
    User.of(hash = hash, datum = datum)
      .hash.value shouldBe "code"
  }

  "User of datum should be datum.value = contents" {
    User.of(hash = hash, datum = datum)
      .datum.value shouldBe "contents"
  }

  "UserData toString method should be UserData(hash=Hash(value=code), datum=Datum(value=contents))" {
    User.of(hash = hash, datum = datum)
      .toString() shouldBe "UserData(hash=Hash(value=code), datum=Datum(value=contents))"
  }

  "User toString method should not be User(empty, empty)" {
    User.toString() shouldNotBe "User(empty, empty)"
  }
})
