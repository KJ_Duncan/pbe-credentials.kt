package validity

import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.comparables.shouldBeEqualComparingTo
import io.kotest.matchers.ints.shouldBeExactly
import io.kotest.matchers.ints.shouldNotBeExactly
import io.kotest.matchers.string.shouldMatch
import io.kotest.matchers.string.shouldNotBeBlank

internal class SecureHashTest : StringSpec() {

  private val charArray: CharArray = charArrayOf('a', 'r', 'a', 'n', 'd', 'o', 'm', 'c', 'h', 'a', 'r')

  init {
    "SecureHash invoke should not be blank" {
      SecureHash(charArray).shouldNotBeBlank()
      // println(SecureHash(charArray))
    }

    "generate hash should match \\w+:\\w+:\\w+" {
      SecureHash(charArray).shouldMatch("\\w+:\\w+:\\w+:\\w+".toRegex())
    }

    "generate hash should have a total length of 20 + 1 + 4 + 1 + 128 + 1 + 64 = 219" {
      SecureHash(charArray).length.shouldBeExactly(219)
    }

    "generate hash should have iteration count of 4096" {
      SecureHash(charArray).split(":")[1].toInt().shouldBeExactly(4096)
    }

    "SecureHash salt length should be 64 * 2 = 128 characters" {
      SecureHash(charArray).split(":")[2].length.shouldBeExactly(64 * 2)
    }

    "SecureHash hash should be length 64" {
      SecureHash(charArray).split(":")[3].length.shouldBeExactly(64)
    }

    "Algorithm should be equal to PBKDF2WithHmacSHA256" {
      SecureHash(charArray).split(":")[0].shouldBeEqualComparingTo("PBKDF2WithHmacSHA256")
    }

    "SecureHash salt should not be padded with 0s" {
      SecureHash(charArray).split(":")[2][0].toInt().shouldNotBeExactly(0)
    }

    "SecureHash hash should not be padded with 0s" {
      SecureHash(charArray).split(":")[3][0].toInt().shouldNotBeExactly(0)
    }
  }
}
