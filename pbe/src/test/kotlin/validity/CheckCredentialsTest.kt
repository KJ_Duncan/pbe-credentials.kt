package validity

import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe

internal class CheckCredentialsTest : StringSpec() {

  private val charArray: CharArray = charArrayOf('a', 'r', 'a', 'n', 'd', 'o', 'm', 'c', 'h', 'a', 'r')
  private val originalHash: String = SecureHash(charArray)

  init {
    "CheckCredentials slow equals returns true on matching passphrase and authentication" {
      CheckCredentials(charArray, originalHash) shouldBe true
    }

    "CheckCredentials slow equals returns false on reversed passphrase" {
      CheckCredentials(charArray.reversedArray(), originalHash) shouldBe false
    }

    "CheckCredentials slow equals returns false on reversed authentication" {
      val hash = originalHash.split(":")
      val reverse = hash[3].reversed()
      val substitute = "${hash[0]}:${hash[1]}:${hash[2]}:$reverse"
      CheckCredentials(charArray, substitute) shouldBe false
    }

    "CheckCredentials slow equals returns false on smaller passphrase" {
      val smaller = charArray.sliceArray(0..3)
      CheckCredentials(smaller, originalHash) shouldBe false
    }

    "CheckCredentials slow equals returns false on larger passphrase" {
      CheckCredentials(charArray.plus('s'), originalHash) shouldBe false
    }
  }
}
