package validity

import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe
import io.kotest.property.checkAll
import io.kotest.property.exhaustive.exhaustive

internal class ValidatorTest : StringSpec() {

  init {
    "validation exception" {
    }

    "validation input chars = [' ', '\t', '\n', '\r']" {
    }

    "validate connection url" {
    }

    /* Now a private function
    "minimum standard regex01 fails on 3 consecutive repeated characters" {
      val strings = listOf(">>>fmG", ">fffmG", ">fmmmG", ">fmGGG", ">fmGQQQ", ">fmGQooo", ">fmGQoZZZ", ">fmGQoZ...").exhaustive()
      checkAll(strings) { str ->
        Validator.minimumStandardBootPasswordValidation(str) shouldBe false
      }
    }

    Now a private function
    "minimum standard regex01 passes on 27 characters" {
      val strings = listOf(">fmGQoZ.R'msaLRfu%!*lM0\"U;", "O@h^3@9n!&toGtHz/bk@IL.{z.", "EF3]|wsR2'c-Eg3u|>z0iyOF;c", ",<{ia.mmZf;j3123cQE[EgfKqBO", "ypqgvi0kYAAF^fbx#}~o4Jp*91", "ABC%*(yuoo)#\$)!##)%^&()'\"re", "{=[R/ql8s>eWP&#,YqTlM]w'YK", "'z~(z:~{\"qWVwY759i`D.0c!", "i\"URg\$Q.DF&?z&\"w%r#&Fn7V{+").exhaustive()
      checkAll(strings) { str ->
        Validator.minimumStandardPasswordValidation(str) shouldBe true
      }
    }
     */
  }
}
