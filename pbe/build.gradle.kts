plugins {
  `java-library`
  kotlin("jvm") version "1.5.31"
}

group = "au.kjd"
version = "1.0-SNAPSHOT"

repositories {
  mavenCentral()
  maven("https://kotlin.bintray.com/kotlinx")
}

/* Order matters as it is deterministic on package compliance */
dependencies {
  constraints {
    implementation("org.jetbrains.kotlinx:kotlinx-collections-immutable-jvm:0.3.4")

    implementation("org.apache.derby:derby:10.15.2.0")
    implementation("org.apache.derby:derbynet:10.15.2.0")

    testImplementation("org.junit.jupiter:junit-jupiter-api:5.7.2")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.7.2")

    testImplementation("io.kotest:kotest-runner-junit5-jvm:4.6.0")
    testImplementation("io.kotest:kotest-assertions-core-jvm:4.6.0")
    testImplementation("io.kotest:kotest-property-jvm:4.6.0")
    testImplementation("io.kotest:kotest-assertions-sql-jvm:4.6.0")
  }

  dependencies {
    implementation("org.jetbrains.kotlinx:kotlinx-collections-immutable-jvm")

    implementation("org.apache.derby:derby")
    implementation("org.apache.derby:derbynet")

    testImplementation("org.junit.jupiter:junit-jupiter-api")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine")

    testImplementation("io.kotest:kotest-runner-junit5-jvm")
    testImplementation("io.kotest:kotest-assertions-core-jvm")
    testImplementation("io.kotest:kotest-property-jvm")
    testImplementation("io.kotest:kotest-assertions-sql-jvm")
  }
}

tasks {
  /* Gradle - Building Modules for the Java Module System
   *        - Java Module System support is an incubating feature
   * https://docs.gradle.org/current/userguide/java_library_plugin.html#sec:java_library_modular
   */
  compileJava {
    options.compilerArgs = listOf("--release", "17")
    // options.javaModuleMainClass.set("")
    java.modularity.inferModulePath.set(true)
  }

  /* As at 17 April 2021, highest generated JVM bytecode 15
   * https://kotlinlang.org/docs/compiler-reference.html#jvm-target-version
   */
  compileKotlin {
    kotlinOptions.jvmTarget = "17"
    kotlinOptions.freeCompilerArgs =
      listOf("-Xinline-classes", "-Xallow-no-source-files", "-Xjvm-enable-preview")
  }

  compileTestKotlin { kotlinOptions.jvmTarget = "17" }

  test {
    useJUnitPlatform()
    jvmArgs = listOf("-ea", "--enable-preview")
    failFast = true
    testLogging.showStandardStreams = true
    testLogging {
      events(
        "passed",
        "failed",
        "skipped",
        "standard_out",
        "standard_error"
      )
    }
  }

  /* FIXME: manifest represents this repository */
  jar {
    enabled = false

    from(sourceSets.main.get().output)
    exclude { details: FileTreeElement ->
      details.file.name.endsWith(".png") ||
      details.file.name.endsWith(".puml")
    }

    /* need to rerun:
     *  $ jar --update --file build/libs/<name>.jar --main-class <fq-name>
     */
    manifest {
      attributes(
        "Manifest-Version" to "1.0",
        "Created-By" to "Kirk Duncan",
        "Name" to "au/kjd/",
        "Sealed" to "true",
        "Main-Class" to "au.kjd.EntryPointKt",
        "Implementation-Title" to "Encrypted credential storage with Derby databases",
        "Implementation-Version" to project.version
      )
    }
  }
}

/* ilya-g 2020, build.gradle.kts, kotlin-jlink-examples, gradle, viewed 01 March 2021,
 *              https://github.com/ilya-g/kotlin-jlink-examples/blob/master/gradle/app/build.gradle.kts
 *
 *  val moduleName by extra("")
 *  val javaHome = System.getProperty("java.home")
 *
 *  register<Exec>("jlink") {
 *    description = "Build kowsky.kak module jar with an optimised custom runtime image"
 *    val outputDir by extra("$buildDir/jrt-kowsky-kak")
 *    inputs.files(configurations.runtimeClasspath)
 *    inputs.files(jar)
 *    outputs.dir(outputDir)
 *    doFirst {
 *      val modulePath = files(jar) + configurations.runtimeClasspath.get()
 *      logger.lifecycle(modulePath.joinToString("\n", "jlink module path:\n"))
 *      delete(outputDir)
 *      commandLine("$javaHome/bin/jlink",
 *                  "--module-path",
 *                  listOf("$javaHome/jmods/", modulePath.asPath).joinToString(File.pathSeparator),
 *                  "--add-modules", moduleName,
 *                  "--output", outputDir
 *      )
 *    }
 *  }
 */
