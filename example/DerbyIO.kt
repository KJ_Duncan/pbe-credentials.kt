package example

import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.lang.System.`in`
import java.sql.Connection
import java.sql.DriverManager
import java.sql.SQLException
import java.sql.Statement

/**
 * This sample program is described in the Getting Started With Derby Manual
 *
 * $ kotlinc -verbose -include-runtime -jvm-target 13 example.DerbyIO.kt
 * $ java -cp $CLASSPATH:$DERBY_HOME/lib/derby.jar example.DerbyIO
 *
 * @author Apache Derby 2020, Activity 3: Run a JDBC program using the embedded driver,
 *         viewed 26 June 2020, http://db.apache.org/derby/docs/10.15/getstart/index.html
 *
 * REMOVE::example.DerbyIO demonstration program prior to production.
 */
object DerbyIO {
  /* ## DEFINE VARIABLES SECTION ##
     Driver to use, the database name,
     Derby connection URL to use */
  private const val dbName: String = "jdbcDemoDB"
  private const val connectionURL: String = "jdbc:derby:$dbName;create=true"
  private const val EXIT: String = "exit"

  private const val printLine: String = "  __________________________________________________"
  private const val createString: String = "CREATE TABLE WISH_LIST  " +
                                           "(WISH_ID INT NOT NULL GENERATED ALWAYS AS IDENTITY " +
                                           "   CONSTRAINT WISH_PK PRIMARY KEY, " +
                                           " ENTRY_DATE TIMESTAMP DEFAULT CURRENT_TIMESTAMP, " +
                                           " WISH_ITEM VARCHAR(32) NOT NULL) "

  /* JDBC code sections
     Beginning of Primary DB access section

     ## BOOT DATABASE SECTION ## */
  private fun go(): Unit = try {

    /* Create (if needed) and connect to the database.
       The driver is loaded automatically. */
    with(DriverManager.getConnection(connectionURL)) {
      println("Connected to database $dbName")

      /* ## INITIAL SQL SECTION ##
         Create a statement to issue simple commands. */
      val statement: Statement = createStatement()
        /* Call utility method to check if table exists.
           Create the table if needed */
        .also { hasTable(statement = it, connection = this) }

      /* Prepare the insert statement to use */
      prepareStatement("insert into WISH_LIST(WISH_ITEM) values (?)")
        .apply { /* ## ADD - DISPLAY RECORD SECTION ##
                    The Add-Record Loop - continues
                    until 'exit' is entered */
          loop@ do {

            /* Call utility method to ask user for input */
            val answer: String = getWishItem()

            /* Check if it is time to EXIT, if not insert the data */
            when {
              answer != EXIT -> {

                /* Insert the text entered into the WISH_ITEM table */
                setString(1, answer)
                executeUpdate()
                println(printLine)

                /* Select all records in the WISH_LIST table */
                statement.executeQuery("select ENTRY_DATE, WISH_ITEM from WISH_LIST order by ENTRY_DATE")
                  .also {

                    /* Loop through the ResultSet and print the data */
                    while (it.next()) println("On ${it.getTimestamp(1)} I wished for ${it.getString(2)}")
                    println(printLine)

                  /* Close the resultSet */
                  }.close()
              }
              else -> continue@loop
            }

          /* Check if it is time to EXIT, if so end the loop */
          } while (answer != EXIT)

        /* Close the prepareStatement */
        }.close()

      /* Release the resources (clean up) */
      release(statement, connection = this)
      shutdown()
    }
  /* Beginning of the primary catch block: prints stack trace */
  } catch (t: Throwable) {
    println(" . . . exception thrown:")
    /* Catch all exceptions and pass them to
       the Throwable.printStackTrace method  */
    t.printStackTrace()
  } finally { println("Getting Started With Derby JDBC program ending.") }

  /* ## DATABASE SHUTDOWN SECTION ##
     In embedded mode, an application should shut down Derby.
     Shutdown throws the XJ015 exception to confirm success. */
  private fun shutdown(): Unit = run {
    var gotSQLExc: Boolean = false

    try { DriverManager.getConnection("jdbc:derby:;shutdown=true") }
    catch (sqlex: SQLException) { if (sqlex.sqlState == "XJ015") gotSQLExc = true }

    if (!gotSQLExc) println("Database did not shut down normally")
    else println("Database shut down normally")
  }

  /* Read input from the user at the command line */
  private fun getWishItem(): String =
    try {
      var input: String = ""

      while (input.isEmpty()) {
        println("Enter wish-list item (enter exit to end): ")
        input = readInput()

        if (input.isEmpty()) println("Nothing entered: ")
      }
      input
    } catch (ioe: IOException) { println("Could not read input from stdin: ${ioe.message}"); "" }

  /* Check for the existence of a derby database */
  private fun checkForTable(connectionTest: Connection): Boolean =
    try {
      connectionTest.createStatement().apply {
        execute("update WISH_LIST set ENTRY_DATE = CURRENT_TIMESTAMP, WISH_ITEM = 'TEST ENTRY' where 1=3")
      }.also { println("Just got the warning - table exists OK ") }
      true
    } catch (sqlex: SQLException) {
      with(sqlex.sqlState) {
        println("  Utils GOT:   $this")
        /* Table does not exist */
        return when (this) {
          "42X05" -> false
          "42X14", "42821" -> { println("checkForTable: Incorrect table definition. Drop table WISH_LIST and rerun this program"); throw sqlex }
          else -> { println("checkForTable: Unhandled SQLException"); throw sqlex }
        }
      }
    }

  // ---------------------- Helper Functions ---------------------- \\
  private fun hasTable(statement: Statement, connection: Connection) {
    if (!checkForTable(connection)) {
      println(" . . . . creating table WISH_LIST")
      statement.execute(createString)
    }
  }

  /* Can iterate over List<AutoClosable> and call T.close() */
  private fun release(statement: Statement, connection: Connection) {
    statement.close()
    connection.close()
    println("Closed connection")
  }

  private fun readInput(): String =
    BufferedReader(InputStreamReader(`in`)).readLine()

  // ------------- Entry Point ------------- \\
  @JvmStatic
  fun main(args: Array<String>): Unit = go()
}
