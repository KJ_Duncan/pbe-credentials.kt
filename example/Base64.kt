package example

import java.util.Base64

/* ------------------
| Type  | Bit Width |
---------------------
  Double     64
  Float      32
  Long       64
  Int        32
  Short      16
  Byte       8
---------------------

Int.SIZE_BYTES = 1
Int.SIZE_BITS = 8

Byte.MIN_VALUE = -128
Byte.MAX_VALUE = 127

hexBytes = 0xFF_EC_DE_5E
   bytes = 0b11010010_01101001_10010100_10010010


Convert decimals to binary: divide by 2 until numerator is 0 keeping a record of the remainder 0 or 1.
0/2=0 1/2=1 3/2=1 7/2=1 15/2=1 30/2=0 61/2=1 123/2=1 <-- left to right <-- 01111011 = 123
123/2=1 61/2=1 30/2=0 15/2=1 7/2=1 3/2=1 1/2=1 0/2=0 --> do it and reverse it <--

Convert Hex to decimal: Hx16^n + Hx16^n-1 ... Hx16^n-n
FFFF 15x16^3+15x16^2+15x16^1+15x16^0 = 61440+3840+240+15 = 65535
39BD 3x16^3+9x16^2+11x16^1+13x16^0 = 12288+2304+176+13 = 14781

-------------------------
| Hex | Binary | Decimal |
--------------------------
| 16^  |  2^   |    10^  |
--------------------------
   0     0000       0       0x16^0
   1     0001       1       1x16^0
   2     0010       2       2x16^0
   3     0011       3
   4     0100       4
   5     0101       5       5/2=1 2/2=0 1/2=1 0/2=0 <-- 0101
   6     0110       6       6/2=0 3/2=1 1/2=1 0/2=0 <-- 0110
   7     0111       7       7/2=1 3/2=1 1/2=1 0/2=0 <-- 0111
   8     1000       8       8/2=0 4/2=0 2/2=0 1/2=1 0/2=0 <-- 01000
   9     1001       9       9/2=1 4/2=0 2/2=0 1/2=1 0/2=0 <-- 01001
   A     1010       10      1x2^3+0x2^2+1x2^1+0x2^0
   B     1011       11
   C     1100       12      1x2^3+1x2^2+0x2^1+0x2^0
   D     1101       13      1x2^3+1x2^2+0x2^1+1x2^0
   E     1110       14      1x2^3+1x2^2+1x2^1+0x2^0
   F     1111       15
--------------------------

Bit operators only apply to integer types: byte, short, int, long.
A character involved in a bit operation is converted to an integer.
All bitwise operators can form bitwise assignment operators: =, |=, <<=, >>=, >>>=.

----------------------------------------------------------------------------------------------------------------
| Operator |    Name           |               Description             |            Examples                   |
----------------------------------------------------------------------------------------------------------------
    and      AND                  yields 1 if both bit are 1                  10101110 and 10010010 == 10000010
    or       OR (inclusive)       yields 1 if either bit is 1                 10101110 or  10010010 == 10111110
    xor      OR (exclusive)       yields 1 if two bits are different          10101110 xor 10010010 == 00111100
    inv      Complement           toggles each bit 0 to 1 and 1 to 0          10101110 inv    == 01010001
    shl      Left shift           shift bits left fill the right with 0       10101110 shl 2  == 10111000
    shr      Right shift          shift bits right fill left with high bit    10101110 shr 2  == 11101011
    ushr     Unsigned right       shift bits left by number on right and      10101110 ushr 2 == 00101011
             shift zero           fill the left side with 0s                  00101110 ushr 2 == 00001011
             extension
----------------------------------------------------------------------------------------------------------------
Reference:
  Liang, Y. D 2015, Number Systems & Bitwise Operations, Appendix F & G,
  Introduction to Java Programming, Comprehensive Version, Tenth Edition,
  pp. 1273-77, Armstrong Atlantic State University, Pearson Publishing. */

/*
 * REMOVE: all content has been copied verbatim from the reference source and marked for deletion prior to production.
 *
 * @author Corda 2020, ByteArrays.kt & EncodingUtils.kt, open-source blockchain, release 4.6, viewed 03 June 2020,
 *         https://github.com/corda/corda/blob/release/os/4.6/core/src/main/kotlin/net/corda/core/utilities/
 */

/* Convert a byte array to a Base64 encoded [String]. */
fun ByteArray.toBase64(): String = Base64.getEncoder().encodeToString(this)

/* Convert a String to a Base64 byte array. */
fun String.base64ToByteArray(): ByteArray = Base64.getDecoder().decode(this)

/* Convert a byte array to a hex (Base16) capitalised encoded [String]. */
fun ByteArray.toHex(): String = toHexString()

/* Encoding changer. Base64-[String] to Hex-[String], i.e. "SGVsbG8gV29ybGQ=" -> "48656C6C6F20576F726C64" */
fun String.base64toHex(): String = base64ToByteArray().toHex()

/* Encoding changer. Hex-[String] to Base64-[String], i.e. "48656C6C6F20576F726C64" -> "SGVsbG8gV29ybGQ=" */
fun String.hexToBase64(): String = hexToByteArray().toBase64()

/* Hex-String to [ByteArray]. Accept any hex form (capitalized, lowercase, mixed). */
fun String.hexToByteArray(): ByteArray = parseAsHex()

/* Converts this ByteArray into a String of hexadecimal digits */
fun ByteArray.toHexString(): String = printHexBinary(this)

private val hexCode: CharArray = "0123456789ABCDEF".toCharArray()

private fun printHexBinary(data: ByteArray): String {
  val sb = StringBuilder(data.size * 2)
  for (b in data) {
    //                                       15
    //                                     1111
    sb.append(hexCode[(b.toInt() shr 4) and 0xF])
    sb.append(hexCode[b.toInt() and 0xF])
  }
  return sb.toString()
}

/* Converts this String to hexadecimal digits into a ByteArray */
fun String.parseAsHex(): ByteArray = parseHexBinary(this)

private fun parseHexBinary(str: String): ByteArray {
  val length = str.length

  /* "111" is not a valid hex encoding */
  if (length % 2 != 0) throw IllegalArgumentException("hexBinary needs to be an even-length: $str")

  val output = ByteArray(length / 2)

  fun hexToBin(char: Char): Int {
    // need to return type Int, preform operation against base case (identity)
    if (char in '0'..'9') return char - '0'
    if (char in 'A'..'F') return char - 'A' + 10
    return if (char in 'a'..'f') char - 'a' + 10
           else -1
  }

  var i = 0

  while (i < length) {
    val h = hexToBin(str[i])
    val l = hexToBin(str[i + 1])

    if (h == -1 || l == -1) throw IllegalArgumentException("contains illegal character for hexBinary: $str")

    output[i / 2] = (h * 16 + l).toByte()

    i += 2
  }

  return output
}
